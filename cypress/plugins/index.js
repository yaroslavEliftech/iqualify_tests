/// <reference types="cypress" />

// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
 const path = require("path");
 const gmail = require("gmail-tester");
 const fs = require('fs');

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  on('before:browser:launch', (browser = {}, launchOptions) => {
    if (browser.family === 'chromium') {
      launchOptions.args.push('--disable-dev-shm-usage');
    }

    return launchOptions;
  });

  on('task', {
    setUserId: (val) => { return (userId = val); },
    getUserId: () => { return userId; },
    "gmail:check": async args => {
      const email  = await gmail.check_inbox(
        path.resolve(__dirname, "credentials.json"),
        path.resolve(__dirname, "gmail_token.json"),
        args.options
      );
      return email;
    },
    "save:pdf": async (email) => {
      let buff = new Buffer(email.data, 'base64');
      fs.writeFileSync(email.filename, buff);
      return true;
    }
  })
};
