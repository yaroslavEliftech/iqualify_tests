// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
// -- This is a Login command --
Cypress.Commands.add("login", () => {
  cy.intercept("/iqualifi-api/api/v1/api-proxy/").as("Options");
  const password = "buddy";
  cy.visit(Cypress.env("url"), {
    failOnStatusCode: false,
  }).then(() => {
    if (Cypress.env("url").includes(`/staging`)) {
      cy.get("input.input--text.w-password-page.w-input").type(password);
      cy.get("input.button.w-password-page.w-button").click();
    }
  });
});


Cypress.Commands.add("loginRetail", () => {
  cy.intercept("/iqualifi-api/api/v1/api-proxy/").as("Options");
  if (Cypress.env("url").includes(`/staging`)) {
    cy.login();
  } else {
    cy.visit("https://www.iqualifi.net/retail", {
      failOnStatusCode: false,
    })
  }
});

Cypress.Commands.add('isRetail', () => {
  return Cypress.env("url").includes(`/staging`) || Cypress.env("url").includes(`/retail`);
});

Cypress.Commands.add("logout", () => {
  cy.reload(true);
});

// Get auth code
Cypress.Commands.add("authenticate_email", () => {
  cy.request({
    method: "POST",
    url: `https://${Cypress.env("url").includes(`/staging`) ? "dev" : "beta"
      }.iqualifi.net/iqualifi-api/api/v1/authenticate-email/`,
    form: true,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Referer: `https://${Cypress.env("url").includes(`/staging`) ? "dev" : "beta"
        }.iqualifi.net/`,
    },
    body: {
      email: "yaroslav.borovets@eliftech.com",
    },
  });
});

Cypress.Commands.add("submit_Inputs", (program, isAlternative = false) => {
  cy.fixture("programs.json").then((programs) => {
    assert.isString(program);
    expect(program).to.be.oneOf(Object.keys(programs));
    const currentProgram = programs[program];
    cy.set_Loan_Amount(currentProgram["Loan Amount"]);
    cy.set_LTV(currentProgram["LTV"]);
    cy.set_gift(currentProgram["Gift"]);
    cy.set_gift_of_equity(
      currentProgram["Gift Of Equity"],
      currentProgram["Gift"]
    );
    cy.set_Veteran(currentProgram["Veteran"]);
    cy.set_Loan_Purpose(currentProgram["Loan Purpose"]);
    cy.set_Occupancy_Type(currentProgram["Occupancy Type"]);
    cy.set_first_time_home_buyer(currentProgram["First time home buyer"]);
    cy.set_state_County(
      `${currentProgram["County"]}`,
      `${currentProgram["Property State"]}`
    );
    cy.set_Property_Type(currentProgram["Property Type"]);
    cy.set_Credit_Score(currentProgram["Credit Score"]);
    cy.set_Housing_History(currentProgram["Housing History"]);
    cy.set_Reserves(currentProgram["Reserves"]);
    cy.set_Employment(currentProgram["Employment"]);
    cy.set_Document_Type(currentProgram["Document Type"]);
    cy.set_DTI(currentProgram["DTI"]);
    cy.set_Citizenship(currentProgram["Citizenship"]);
    cy.contains("SUBMIT").click();
    cy.wait("@Options");
    if (isAlternative) {
      cy.verify_Alternative_Program_Name(currentProgram["Full Name"]);
    } else {
      cy.verify_Recommended_Program_Name(currentProgram["Full Name"]);
      cy.verify_Alternative_Program_Name(currentProgram["Alternative Program"]);
    }
  });
});

// -- This set Loan Amount field --
Cypress.Commands.add("set_Loan_Amount", (loan) => {
  assert.isNumber(loan);
  cy.get("#displayedText").type(loan);
});

// -- This set LTV field --
Cypress.Commands.add("set_LTV", (ltv) => {
  assert.isNumber(ltv);
  cy.get('[name="ltv"]').clear().type(ltv);
});

// -- This set ASSETS INCLUDE A GIFT? field --
Cypress.Commands.add("set_gift", (bool) => {
  assert.isBoolean(bool);
  cy.contains(`Assets include a gift?`)
    .parent()
    .within(() => {
      cy.get(bool ? `[value="Yes"]` : `[value="No"]`).click({
        force: true,
      });
    });
});

// -- This set ASSETS INCLUDE A GIFT? field --
Cypress.Commands.add("set_Veteran", (bool) => {
  if (typeof bool !== "undefined") {
    assert.isBoolean(bool);
    cy.contains(`VETERAN ?`)
      .parent()
      .within(() => {
        cy.get(bool ? `[value="Yes"]` : `[value="No"]`).click({
          force: true,
        });
      });
  }
});

// -- This set Gift of Equity field --
Cypress.Commands.add("set_gift_of_equity", (bool, gift) => {
  assert.isBoolean(bool);
  if (gift) {
    cy.contains(`Is this a gift of equity?`)
      .parent()
      .within(() => {
        cy.get(bool ? `[value="Yes"]` : `[value="No"]`).click({
          force: true,
        });
      });
  }
});

// -- This set Loan Purpose field --
Cypress.Commands.add("set_Loan_Purpose", (purpose) => {
  expect(purpose).to.be.oneOf([
    "IRRRL",
    "Cashout Type 1",
    "Cashout Type 2",
    "Purchase",
    "Rate and Term Refinance",
    "Cash-Out Refinance",
  ]);
  cy.get(`[value="${purpose}"]`).click();
});

// -- This set First time home buyer? field --
Cypress.Commands.add("set_first_time_home_buyer", (bool) => {
  assert.isBoolean(bool);
  cy.contains(`First time home buyer?`)
    .parent()
    .within(() => {
      cy.get(bool ? `[value="Yes"]` : `[value="No"]`).click({
        force: true,
      });
    });
});

// -- This set Occupancy Type field --
Cypress.Commands.add("set_Occupancy_Type", (type) => {
  expect(type).to.be.oneOf([
    "Primary",
    "Second Home or Vacation",
    "Investment",
  ]);
  cy.get(`[value="${type}"]`).click();
});

// -- This set County field --
Cypress.Commands.add("set_state_County", (county, state) => {
  assert.isString(county);
  const searchTxt = state ? `li:contains("${county}, ${state}")` : `li:contains("${county}}")`;
  cy.get("input.number-of-units.dd-with-search:nth-child(2)")
    .clear()
    .type(county)
    .get(searchTxt, { timeout: 10000 })
    .click({});
});

// -- This set County field --
Cypress.Commands.add("set_Property_State", (state) => {
  assert.isString(state);
  cy.contains(`STATE`)
    .parent()
    .within(() => {
      cy.get(".dropdown-body")
        .click({
          force: true,
        })
        .get(`li:contains("${state}")`)
        .click({
          force: true,
        });
    });
});

// -- This set Credit score field --
Cypress.Commands.add("set_Credit_Score", (creditScore) => {
  const SCORES = [
    "800 and above",
    "780 to 799",
    "760 to 779",
    "740 to 759",
    "720 to 739",
    "700 to 719",
    "680 to 699",
    "660 to 679",
    "650 to 659",
    "640 to 649",
    "620 to 639",
    "600 to 619",
    "580 to 599",
    "560 to 579",
    "540 to 559",
    "Less than 540",
    "No Credit Score (foreign borrowers only)",
  ];
  assert.isString(creditScore);
  expect(creditScore).to.be.oneOf(SCORES);
  cy.get("#credit_score")
    .click({
      force: true,
    })
    .get(`li:contains("${creditScore}")`)
    .click({
      force: true,
    });
});

// -- This set Credit score field --
Cypress.Commands.add("set_Reserves", (reserves) => {
  const RESERVES = [
    "More than 110% of loan principal",
    "24 months or more",
    "23",
    "22",
    "21",
    "20",
    "19",
    "18",
    "17",
    "16",
    "15",
    "14",
    "13",
    "12",
    "11",
    "10",
    "9",
    "8",
    "7",
    "6",
    "5 months or less",
  ];
  assert.isString(reserves);
  expect(reserves).to.be.oneOf(RESERVES);

  cy.contains(`RESERVES`)
    .parent()
    .within(() => {
      cy.get(".dropdown-body")
        .click({
          force: true,
        })
        .get(`li:contains("${reserves}")`)
        .click({
          force: true,
        });
    });
});

// -- This set EMPLOYMENT score field --
Cypress.Commands.add("set_Employment", (employment) => {
  const EMPLOYMENTS = [
    "Borrower(s) are salaried (W2) employees",
    "One or More Borrowers is Self-Employed",
    "Other - Borrower(s) are Wage Earners",
  ];
  assert.isString(employment);
  expect(employment).to.be.oneOf(EMPLOYMENTS);
  cy.get("#self_employed_status")
    .click({
      force: true,
    })
    .get(`li:contains("${employment}")`)
    .click({
      force: true,
    });
});

// -- This set Document Type score field --
Cypress.Commands.add("set_Document_Type", (document) => {
  const DOCUMENTS = [
    "Pay Stub or W2 or Tax Returns",
    "AUS Express",
    "Bank Statement Income",
    "Asset Depletion",
    "Proof of Rental Income",
    "1099s",
    "Written VOE"
  ];
  const DocumentWithAdditionalInfo = [
    "Proof of Rental Income",
    "1099s",
    "Bank Statement Income",
  ];
  assert.isString(document[0]);
  expect(document[0]).to.be.oneOf(DOCUMENTS);
  cy.get("#document_type")
    .click({
      force: true,
    })
    .get(`li:contains("${document[0]}")`)
    .click({
      force: true,
    });
  if (DocumentWithAdditionalInfo.includes(document[0])) {
    if (document[0] === DocumentWithAdditionalInfo[0]) {
      expect(document[1]).to.be.oneOf(["Investor Occupancy", "Owner Occupied"]);
      cy.contains(`INCOME TYPE`)
        .parent()
        .parent()
        .within(() => {
          cy.get(".dropdown-body")
            .click({
              force: true,
            })
            .get(`li:contains("${document[1]}")`)
            .click({
              force: true,
            });
        });
    } else if (document[0] === DocumentWithAdditionalInfo[1]) {
      cy.get('[name="1099s_in_years"]').clear().type(document[1]);
    } else if (document[0] === DocumentWithAdditionalInfo[2]) {
      cy.get('[name="bank_statement_in_months"]').clear().type(document[1]);
    }
  }
});

// -- This set DTI score field --
Cypress.Commands.add("set_DTI", (dti) => {
  const arrayDTI = dti.split(" ");
  const currency = arrayDTI[0];
  const number = parseInt(arrayDTI[1].slice(0, arrayDTI[1].length - 1));
  expect(currency).to.be.oneOf(["DTI", "DSC"]);
  if (currency === "DTI") {
    assert.isNumber(number);
    assert.isAbove(number, 19); //strict
    assert.isBelow(number, 66); //strict
    cy.get('[value="Debt to Income (DTI)"]').click({
      force: true,
      multiple: true,
    });
    cy.get('[name="dti_dsc_ratio"]').clear().type(number);
  } else if (currency === "DSC") {
    assert.isNumber(number);
    if (number === 0) {
      cy.get(`[value="Debt Service Coverage (DSC)"]`).click({
        force: true,
        multiple: true,
      });
      cy.get(`[value="No ratio"]`).click({
        force: true,
        multiple: true,
      });
    } else {
      assert.isAbove(number, 48); //strict
      assert.isBelow(number, 131); //strict
      cy.get(`[value="Debt Service Coverage (DSC)"]`).click({
        force: true,
        multiple: true,
      });
      cy.get(`[value="Enter ratio"]`).click({
        force: true,
        multiple: true,
      });
      cy.get('[name="dti_dsc_ratio"]').clear().type(number);
    }
  }
});

// -- This set Property Type score field --
Cypress.Commands.add("set_Property_Type", (array) => {
  let units, type;
  const PROPERTIES = [
    "Single Family Detached",
    "Single Family Attached or PUD",
    "Duplex - 2 Family",
    "Triple - 3 Family",
    "4 or more res units",
    "Mixed Use",
    "Warrantable Condo",
    "Non-Warrantable Condo",
    "Condotel",
    "Co-op",
  ];
  const UNITS = [
    "4 or more res units",
    "Mixed Use",
  ];
  const STORIES = [
    "Warrantable Condo",
    "Non-Warrantable Condo",
  ];

  if (Array.isArray(array)) {
    type = array[0];
    units = array[1];
  } else {
    type = array
  }

  assert.isString(type);
  expect(type).to.be.oneOf(PROPERTIES);
  cy.get("#property_type")
    .click({
      force: true,
    })
    .get(`li:contains("${type}")`).eq(0)
    .click({
      force: true,
    }).then(() => {
      if (UNITS.includes(type)) {
        assert.isString(units);
        cy.set_Units(units);
      } else if (STORIES.includes(type)) {
        assert.isString(units);
        cy.set_Stories(units);
      }
    })

});

// -- This set Units score field --
Cypress.Commands.add("set_Units", (units) => {
  if (typeof units === "string") {
    const UNITS = ["2", "3", "4", "5", "6", "7", "8", "9", "10"];
    assert.isString(units);
    expect(units).to.be.oneOf(UNITS);
    cy.get("#units")
      .click({
        force: true,
      })
      .get(`#${units}`)
      .click({
        force: true,
      });
  }
});

// -- This set Units score field --
Cypress.Commands.add("set_Stories", (units) => {
  if (typeof units === "string") {
    const UNITS = ["1", "2", "3", "4", "5", "6 or more",];
    assert.isString(units);
    expect(units).to.be.oneOf(UNITS);
    cy.get("#condo_stories")
      .click({
        force: true,
      })
      .get(`#${units}`)
      .click({
        force: true,
      });
  }
});

// -- This set type code field --
Cypress.Commands.add("type_Code", (code) => {
  assert.isArray(code);
  cy.get("input[name=first]").type(code[0]);
  cy.get("input[name=second]").type(code[1]);
  cy.get("input[name=third]").type(code[2]);
  cy.get("input[name=forth]").type(code[3]);
});

// -- This set HOUSING HISTORY score field --
Cypress.Commands.add("set_Housing_History", (housing) => {
  const HOUSING_HISTORY = [
    "0 x 30 x 24",
    "1 x 30 x 24 with 0 x 30 x 6",
    "0 x 30 x 12",
    "1 x 30 x 12 with 0 x 30 x 6",
    "1 x 30 x 12",
    "2 or more 30 x 12",
    "1 x 60 x 12",
    "2 or more 60 x 12",
  ];
  assert.isString(housing);
  expect(housing).to.be.oneOf(HOUSING_HISTORY);
  cy.get("#housing_history")
    .click({
      force: true,
    })
    .get(`li:contains("${housing}")`)
    .click({
      force: true,
    });
});

// -- This set OWNERSHIP score field --
Cypress.Commands.add("set_Ownership", (ownership) => {
  const OWNERSHIP = [
    "Personally Owned by Borrowers",
    "Owned by Borrowers Through An LLC or Corporation",
  ];
  assert.isString(ownership);
  expect(ownership).to.be.oneOf(OWNERSHIP);
  cy.get("#ownership")
    .click({
      force: true,
    })
    .get(`li:contains("${ownership}")`)
    .click({
      force: true,
    });
});

// -- This set CITIZENSHIP score field --
Cypress.Commands.add("set_Citizenship", (citizenship) => {
  const CITIZENSHIP = [
    "All borrowers are US citizens",
    "One or more borrowers is a foreigner",
    "ITIN - Resident Alien",
    "ITIN - Foreign national"
  ];
  assert.isString(citizenship);
  expect(citizenship).to.be.oneOf(CITIZENSHIP);
  cy.get("#citizenship")
    .click({
      force: true,
    })
    .get(`li:contains("${citizenship}")`)
    .click({
      force: true,
    });
});

// -- This set Credit Event score field --
Cypress.Commands.add("set_Credit_Event", (bool) => {
  assert.isBoolean(bool);
  cy.contains(`CREDIT EVENT IN LESS THAN 7 YEARS?`)
    .parent()
    .within(() => {
      cy.get(bool ? "#yes" : "#no").click();
    });
});

// -- This set Escrow Waiver score field --
Cypress.Commands.add("set_Escrow_Waiver", (escrow, save) => {
  const CHECKBOX = ["Yes", "No"];
  assert.isString(escrow);
  expect(escrow).to.be.oneOf(CHECKBOX);
  cy.contains(`Escrow waiver`)
    .parent()
    .within(() => {
      cy.get(`[value=${escrow}]`)
        .check({ force: true });
    });
  if (save) {
    cy.save_Changes();
  }
});

// -- This set SUBORDINATE FINANCING score field --
Cypress.Commands.add("set_Subordinate_Financing", (financing, cltv, hcltv) => {
  const FINANCING = ["None", "HELOC", "HELOAN"];
  assert.isString(financing);
  expect(financing).to.be.oneOf(FINANCING);
  cy.get(
    ".js-badger-accordion-panel-inner > .dropdown-container > .dropdown-body"
  ).click({
    force: true,
  });
  cy.get(`.dropdown-menu.visible`)
    .within(() => {
      cy.get(`li:contains("${financing}")`).click({
        force: true,
      });
    })
    .then(() => {
      if (financing === "HELOAN") {
        cy.set_CLTV(cltv);
      } else if (financing === "HELOC") {
        cy.set_CLTV(cltv);
        cy.set_HCLTV(hcltv);
      }
    });
});

// -- This set CLTV field --
Cypress.Commands.add("set_CLTV", (cltv) => {
  assert.isNumber(cltv);
  cy.get(
    ".subordinate-financing-options-wrapper > .dropdown-container > .dropdown-body"
  )
    .eq(0)
    .click({
      force: true,
    });
  cy.get(`.dropdown-menu.visible`).within(() => {
    cy.get(`li:contains("${cltv}")`).click({
      force: true,
    });
  });
});

// -- This set HCLTV field --
Cypress.Commands.add("set_HCLTV", (hcltv) => {
  assert.isNumber(hcltv);
  cy.get(
    ".subordinate-financing-options-wrapper > .dropdown-container > .dropdown-body"
  )
    .eq(1)
    .click({
      force: true,
    });
  cy.get(`.dropdown-menu.visible`).within(() => {
    cy.get(`li:contains("${hcltv}")`).click({
      force: true,
    });
  });
});

// -- This is a Get pricing command --
Cypress.Commands.add("get_pricing", (email, isAlternative, waitForEnd = true) => {
  waitForEnd =
    waitForEnd === undefined || waitForEnd === null ? true : waitForEnd;
  cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
  let code;
  if (isAlternative) {
    cy.get('#get-pricing-alt').click();
  } else {
    cy.get("#get-pricing-rec").click();
  }
  cy.get("input[type=text][name=email]").type(email);
  cy.get("button[type=submit]").click();
  cy.contains(`You’ve Got Mail!`);
  cy.request({
    method: "POST",
    url: `https://${Cypress.env("url").includes(`/staging`) ? "dev" : "beta"
      }.iqualifi.net/iqualifi-api/api/v1/get-auth-code`,
    form: true,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Referer: "https://beta.iqualifi.net/",
    },
    retryOnStatusCodeFailure: true,
    body: {
      email: email,
    },
    timeout: 10000,
  }).then((response) => {
    code = String(response.body.code).split("");
    cy.type_Code(code);
  });
  cy.get("button[type=submit]")
    .click()
    .then(() => {
      if (waitForEnd) {
        cy.wait("@getPrice").its('response.statusCode').should('eq', 200)
      }
    });
});

Cypress.Commands.add("email_Recommended_Program_Summary", (email) => {
  cy.intercept("/iqualifi-api/api/v1/pdf/iqualifi/").as("pdfRecommended");
  cy.get('#results-recommended > .container > .email-me-pdf-summary > .recommended-inner-button > span').click();
  cy.get(".modal-container").should("to.be.visible");
  cy.get('input[name="first_name"]').type("broker");
  cy.get('input[name="last_name"]').type("iqualifi");
  cy.get('input[name="email_address"]').type(email);
  cy.get(".checked").click();
  cy.get('div[name="slide-fade"] > .submit-pdf-button').click();
  cy.wait("@pdfRecommended");
});

Cypress.Commands.add("email_Alternative_Program_Summary", (email) => {
  cy.intercept("/iqualifi-api/api/v1/pdf/iqualifi/").as("pdfAlternative");
  cy.get('#results-alternative > .container > .email-me-pdf-summary > .recommended-inner-button').click();
  cy.get(".modal-container").should("to.be.visible");
  cy.get('input[name="first_name"]').type("broker");
  cy.get('input[name="last_name"]').type("iqualifi");
  cy.get('input[name="email_address"]').type(email);
  cy.get(".checked").click();
  cy.get('div[name="slide-fade"] > .submit-pdf-button').click();
  cy.wait("@pdfAlternative");
});

Cypress.Commands.add("select_Price_By_Table_Value", (tableNumber, value) => {
  cy.get(".pricing-table-container")
    .eq(tableNumber)
    .within(() => {
      cy.get(".clickable").contains(value).click();
    });
});

Cypress.Commands.add("get_First_Price_By_Table", (tableNumber) => {
  cy.get(".pricing-table-container")
    .eq(tableNumber)
    .within(() => {
      cy.get(".clickable").contains(/\d+.\d+/).eq(0).click();
    });
});

Cypress.Commands.add("email_Pricing_Summary", () => {
  cy.intercept("/iqualifi-api/api/v1/pdf/summary/").as("summary");
  cy.contains("PRICING SUMMARY").click();
  cy.get(".modal-container").should("to.be.visible");
  // cy.get('input[name="first_name"]')
  //   .type("broker")
  // cy.get('input[name="last_name"]')
  //   .type("iqualifi")
  // cy.get('input[name="email_address"]')
  //   .type(email)
  cy.get('div[name="slide-fade"] > .submit-pdf-button').click();

  cy.wait("@summary");
});

Cypress.Commands.add("email_Pricing_Scenario_Summary_To_AE", (email) => {
  cy.intercept("/iqualifi-api/api/v1/pdf/account-executive-summary").as(
    "aeSummary"
  );
  cy.contains("SCENARIO SUMMARY TO AE").click();
  cy.get(".modal-container").should("to.be.visible");
  cy.get(".invite-guest-input").type(email);
  cy.get('div[name="slide-fade"] > .submit-pdf-button').click();
  cy.wait("@aeSummary");
});

Cypress.Commands.add("email_Pricing_Pre_Quad", (email) => {
  cy.intercept("/iqualifi-api/api/v1/pdf/pre-qual").as("preQuad");
  cy.contains("PRE-QUALIFICATION").click();
  cy.get(".modal-container").should("to.be.visible");
  cy.get('div[name="slide-fade"] > .submit-pdf-button').click();

  cy.wait("@preQuad");
});

Cypress.Commands.add("email_Pricing_Pre_Quad_DR", (data) => {


  cy.intercept("/iqualifi-api/api/v1/pdf/pre-qual").as("preQuad");
  cy.contains("PRE-QUALIFICATION").click();
  cy.get(".modal-container").should("to.be.visible");

  cy.get(`.prequal-pdf-modal-container`).within(() => {
    cy.contains(`First Name`)
      .parent()
      .within(() => {
        cy.get('input').type(data.firstName);
      });

    cy.contains(`Last Name`)
      .parent()
      .within(() => {
        cy.get('input').type(data.lastName);
      });

    cy.contains(`Address:`)
      .parent()
      .within(() => {
        cy.get('input').type(data.address);
      });

    cy.contains(`City`)
      .parent()
      .within(() => {
        cy.get('input').type(data.city);
      });

    cy.contains(`State`)
      .parent()
      .within(() => {
        cy.get(".dropdown-body")
          .click({
            force: true,
          })
          .get(`li:contains("${data.state}")`)
          .click({
            force: true,
          });
      });

    cy.contains(`Zip Code`)
      .parent()
      .within(() => {
        cy.get('input').type(data.zipCode);
      });



    cy.get('div[name="slide-fade"] > .submit-pdf-button').click();
  });
  cy.wait("@preQuad");
});

Cypress.Commands.add("email_which_Programs", (email) => {
  cy.intercept("/iqualifi-api/api/v1/pdf/near-miss-summary").as(
    "nearMissSummary"
  );
  cy.get(".base > .recommended-inner-button").click();
  cy.get(".email-button").click();
  cy.get(".modal-container").should("to.be.visible");
  cy.get('input[name="first_name"]').type("broker");
  cy.get('input[name="last_name"]').type("iqualifi");
  cy.get('input[name="email_address"]').type(email);
  cy.get(".checked").click();
  cy.get(".submit-pdf-button").click();
  cy.wait("@nearMissSummary");
});

// -- This set Pricing_Tear in Guest form --
Cypress.Commands.add("set_Pricing_Tear", (tear) => {
  assert.isString(tear);
  cy.get("#correspondentTier").click({
    force: true,
  });
  cy.get(`li:contains("${tear}")`).click({
    force: true,
  });
  cy.get("#correspondentTier").click({
    force: true,
  });
});

// -- This set Markup for Guest form --
Cypress.Commands.add("set_Mark_Up", (markup) => {
  const MARKUPS = ["Single", "Individual"];
  assert.isString(markup);
  expect(markup).to.be.oneOf(MARKUPS);
  cy.get("#currentMarkup")
    .click({
      force: true,
    })
    .get(`li:contains("${markup}")`)
    .click({
      force: true,
    });
});

Cypress.Commands.add("invite_Guest", (guest) => {
  cy.get(".invite-btn", {
    timeout: 10000,
  }).click();
  cy.get(".guest-modal-container").should("to.be.visible");
  cy.get(`[name="email"]`).eq(0).type(guest.email);
  cy.get(`[name="confirmEmail"]`).type(guest.email);
  cy.get(`[name="firstName"]`).type(guest.firstName);
  cy.get(`[name="lastName"]`).type(guest.lastName);
  cy.get(`[name="companyName"]`).type(guest.companyName);
  cy.get(`[name="phone"]`).type(guest.phone);
  cy.get(`[name="city"]`).type(guest.city);
  cy.get(`#correspondent`).click();
  cy.set_Property_State(guest.state);
  cy.set_Pricing_Tear("Tier 4");
  cy.set_Mark_Up("Individual");

  cy.get(`[name="A11"]`).type(guest.price.A11);
  cy.get(`[name="A14"]`).type(guest.price.A14);
  cy.get(`[name="A16"]`).type(guest.price.A16);

  cy.get(`[name="C Coll"]`).type(guest.price.C_COLLECTION);
  cy.get(`[name="E Coll"]`).type(guest.price.E_COLLECTION);
  cy.get(`[name="F Coll"]`).type(guest.price.F_COLLECTION);
  cy.get(`[name="A Coll"]`).type(guest.price.A_COLLECTION);

  if (cy.isRetail()) {
    cy.get(`[name="V Coll"]`).type(guest.price.V_COLLECTION);
    cy.get(`[name="V3V4 Coll"]`).type(guest.price.V2_COLLECTION);
  }

  cy.get(`[name="E3"]`).type(guest.price.E3);
  cy.get(`[name="E4"]`).type(guest.price.E4);

  cy.get(`[name="I10"]`).type(guest.price.I10);
  cy.get(`[name="I11"]`).type(guest.price.I11);
  cy.get(`[name="I12"]`).type(guest.price.I12);
  cy.get(`[name="I14"]`).type(guest.price.I14);
  cy.get(`[name="I15"]`).type(guest.price.I15);

  cy.get(`[name="J10"]`).type(guest.price.J10);
  cy.get(`[name="J11"]`).type(guest.price.J11);
  cy.get(`[name="J12"]`).type(guest.price.J12);
  cy.get(`[name="M10"]`).type(guest.price.M10);
  cy.get(`[name="R10"]`).type(guest.price.R10);
  cy.get('div[name="slide-fade"] > .submit-pdf-button').click();
});

Cypress.Commands.add("verify_Recommended_Program_Name", (names) => {
  if (Array.isArray(names)) {
    cy.get("#results-recommended > * .title-content-text", {
      timeout: 10000,
    }).as("titlesRec");

    cy.get("@titlesRec").eq(0).should("contain", names[0]);
    cy.get("@titlesRec").eq(1).should("contain", names[1]);
  } else {
    cy.get("#results-recommended > * .title-content-text", {
      timeout: 10000,
    }).should("contain", names);
  }
});

Cypress.Commands.add("verify_Alternative_Program_Name", (alternativesList) => {
  if (Array.isArray(alternativesList)) {
    if (alternativesList.length > 0) {
      cy.fixture("programs.json").then((programs) => {
        const AlternativeName = programs[alternativesList[0]]["Full Name"];
        if (Array.isArray(AlternativeName)) {
          cy.get("#results-alternative > * .title-content-text", {
            timeout: 10000,
          }).as("titlesAlt");

          cy.get("@titlesAlt").eq(0).should("contain", AlternativeName[0]);

          cy.get("@titlesAlt").eq(1).should("contain", AlternativeName[1]);
        } else {
          cy.get("#results-alternative > * .title-content-text", {
            timeout: 10000,
          }).should("contain", AlternativeName);
        }
      });
    }
  }
});

Cypress.Commands.add("set_Price_Option_Amortizing", (opt, save) => {
  const CHECKBOX = ["Amortizing", "Interest only"];
  assert.isString(opt);
  expect(opt).to.be.oneOf(CHECKBOX);
  cy.contains(opt)
    .parent()
    .within(() => {
      cy.get('input')
        .check();
    });
  if (save) {
    cy.save_Changes();
  }
});

Cypress.Commands.add("set_Price_Option_Fees", (opt, save) => {
  const CHECKBOX = ["Fees in", "Fees out"];
  assert.isString(opt);
  expect(opt).to.be.oneOf(CHECKBOX);
  cy.contains(opt)
    .parent()
    .within(() => {
      cy.get('input')
        .check({ force: true });
    });
  if (save) {
    cy.save_Changes();
  }
});

Cypress.Commands.add("set_Mortgage_Insurance", (opt) => {
  const CHECKBOX = ["Yes", "No"];
  assert.isString(opt);
  expect(opt).to.be.oneOf(CHECKBOX);
  cy.contains(`MORTGAGE INSURANCE`)
    .parent()
    .within(() => {
      cy.get(`#${opt}`)
        .check({ force: true });
    });
});

Cypress.Commands.add("set_Rate_Sheet", (opt, save) => {
  const CHECKBOX = ["Commercial", "Correspondent"];
  assert.isString(opt);
  expect(opt).to.be.oneOf(CHECKBOX);
  cy.contains(`Rate sheet`)
    .parent()
    .within(() => {
      cy.get(`#${opt}`)
        .check({ force: true });
    });
  if (save) {
    cy.save_Changes();
  }
});

Cypress.Commands.add("set_Price_Option_Show_Price_By", (price, save) => {
  const PRICES = ["1", "0.5", "0.25", "0.125"];
  assert.isString(price);
  expect(price).to.be.oneOf(PRICES);
  cy.contains(price)
    .parent()
    .within(() => {
      cy.get('input')
        .check({ force: true });
    });
  if (save) {
    cy.save_Changes();
  }
});

Cypress.Commands.add("set_Price_Option_Lock_Days", (days, save) => {
  const DAYS = ["20", "30", "45"];
  assert.isString(days);
  expect(days).to.be.oneOf(DAYS);

  cy.contains(`${days}-day lock`)
    .parent()
    .within(() => {
      cy.get('input')
        .check({ force: true });
    });
  if (save) {
    cy.save_Changes();
  }
});

Cypress.Commands.add("set_Prospect_Type", (type, wait) => {
  const TYPES = [
    "Broker",
    "Correspondent",
    "Commercial",
    "DTC",
    "Distributed Retail",
    "Distributed Retail Manager",
    "Joint Venture",
    "Jvm",
  ];
  assert.isString(type);
  expect(type).to.be.oneOf(TYPES);

  cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
  cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
    cy.get(".dropdown-body").click();
  })
  if (type == "Distributed Retail") {
    cy.get(`li:contains("${"Distributed Retail"}")`).eq(0).click();
  } else if (type == "Distributed Retail Manager") {
    cy.get(`li:contains("${"Distributed Retail"}")`).eq(1).click();
  }
  else {
    cy.get(`li:contains("${type}")`).click();
  }
  if (wait) {
    cy.wait("@getPrice");
  }
});

Cypress.Commands.add("set_Accounts", (type, wait) => {
  assert.isString(type);
  cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
  cy.contains(`ACCOUNTS`).parent().parent().within(() => {
    cy.get(".dropdown-body").click();
  })
  cy.get(`li:contains("${type}")`).click();
  if (wait) {
    cy.wait("@getPrice");
  }
});

Cypress.Commands.add("increase_LPC_Equal_Value", (wait) => {
  cy.get(`.incr-btn`).click({
    force: true,
  });
  if (wait) {
    cy.wait("@getPrice");
  }
});

Cypress.Commands.add("decrease_LPC_Equal_Value", (wait) => {
  cy.get(`.decrement-btn`).click({
    force: true,
  });
  if (wait) {
    cy.wait("@getPrice");
  }
});

Cypress.Commands.add("enter_Client_Mode", () => {
  cy.get(`.employee-dropdown-controller > .text-container`).type(
    `Sanford Mortgage Corporation`
  );
  cy.get(".employee-dropdown-field > .text-container").click({
    force: true,
  });
  cy.wait("@getPrice");
  cy.get(`.mode-text`).should("contain", `Client mode`);
});

Cypress.Commands.add("enter_Employee_Mode", () => {
  cy.get(`.employee-close-search-icon`).click({
    force: true,
  });
  cy.wait("@getPrice");
  cy.get(`.mode-text`).should("contain", `Employee mode`);
});

// -- This set Prepayment Penalty" score field --
Cypress.Commands.add("set_Prepayment_Penalty", (ppp, save) => {
  cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
  const PPP = ["3", "2", "1", "0"];
  assert.isString(ppp);
  expect(ppp).to.be.oneOf(PPP);
  cy.contains(`PREPAYMENT PENALTY OPTIONS`)
    .parent()
    .parent()
    .within(() => {
      cy.get(".dropdown-body")
        .click({
          force: true,
        })
        .get(`li:contains("${ppp}")`)
        .click({
          force: true,
        });
    });
  if (save) {
    cy.save_Changes();
  }
});


Cypress.Commands.add("save_Changes", () => {
  cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
  cy.contains(`UPDATE PRICING TABLE`).click();
  cy.wait("@getPrice");
});

Cypress.Commands.add("set_Pricing_Type", (type, save) => {
  const TYPES = ["BPC", "LPC", "BOTH"];
  assert.isString(type);
  expect(type).to.be.oneOf(TYPES);
  cy.contains(type)
    .parent()
    .within(() => {
      cy.get('input')
        .check({ force: true });
    });
  if (save) {
    cy.save_Changes();
  }
});

Cypress.Commands.add(
  "verify_First_Table_with_All_prices_CUSTOM",
  (programs, columnsIndexes, HEADERS) => {
    //Verify first table
    cy.get(`:nth-child(1) > .pricing-table tr`)
      .not(`[style="display: none;"]`)
      .each(($row) => {
        cy.wrap($row).within(() => {
          cy.get("td").each(($cell, index) => {
            if (columnsIndexes.includes(index)) {
              cy.wrap($cell)
                .invoke("text")
                .then((text) => {
                  if (text.includes('$')) {
                    cy.wrap(text.replaceAll(/[,$]/g, ''))
                  } else {
                    cy.wrap(text)
                  }
                })
                .then(parseFloat)
                .should((value) => {
                  assert.isNumber(value);
                  assert.isNotNaN(value);
                });
            }
            if (index === 1) {
              cy.wrap($cell)
                .invoke("text")
                .should((value) => {
                  assert.isString(value);
                  assert.oneOf(value.trim(), programs);
                });
            }
          });
        });
      });
    //Verify first table headers
    cy.get(`:nth-child(1) > .pricing-table thead td`)
      .each(($row, index) => {
        cy.wrap($row)
          .invoke("text")
          .should((value) => {
            assert.isString(value);
            assert.include(value.trim(), HEADERS[index].trim());
          });
      });
  }
);

Cypress.Commands.add(
  "verify_Second_Table_with_All_prices_CUSTOM",
  (programs, columnsIndexes, HEADERS2) => {
    //Verify second table
    cy.get(`:nth-child(2) > .pricing-table tr`)
      .not(`[style="display: none;"]`)
      .each(($row) => {
        cy.wrap($row).within(() => {
          cy.get("td").each(($cell, index) => {
            if (columnsIndexes.includes(index)) {
              cy.wrap($cell)
                .invoke("text")
                .then((text) => {
                  if (text.includes('$')) {
                    cy.wrap(text.replaceAll(/[,$]/g, ''))
                  } else {
                    cy.wrap(text)
                  }
                })
                .then(parseFloat)
                .should((value) => {
                  assert.isNumber(value);
                  assert.isNotNaN(value);
                });
            }
            if (index === 1) {
              cy.wrap($cell)
                .invoke("text")
                .should((value) => {
                  assert.isString(value);
                  assert.oneOf(value.trim(), programs);
                });
            }
          });
        });
      });
    //Verify first second table headers
    cy.get(`:nth-child(2) > .pricing-table thead td`)
      .each(($row, index) => {
        cy.wrap($row)
          .invoke("text")
          .should((value) => {
            assert.isString(value);
            assert.include(value.trim(), HEADERS2[index].trim());
          });
      });
  }
);

Cypress.Commands.add(
  "verify_Second_Table_Payments_CUSTOM",
  (columnsIndexes, HEADERS2) => {
    //Verify second table
    cy.get(`:nth-child(2) > .pricing-table tr`)
      .not(`[style="display: none;"]`)
      .each(($row) => {
        cy.wrap($row).within(() => {
          cy.get("td").each(($cell, index) => {
            if (columnsIndexes.includes(index)) {
              cy.wrap($cell)
                .invoke("text")
                .then((text) => {
                  if (text.includes('$')) {
                    cy.wrap(text.replaceAll(/[,$]/g, ''))
                  } else {
                    cy.wrap(text)
                  }
                })
                .then(parseFloat)
                .should((value) => {
                  assert.isNumber(value);
                  assert.isNotNaN(value);
                });
            }
          });
        });
      });
    //Verify first second table headers
    cy.get(`:nth-child(2) > .pricing-table thead td`)
      .each(($row, index) => {
        cy.wrap($row)
          .invoke("text")
          .should((value) => {
            assert.isString(value);
            assert.include(value.trim(), HEADERS2[index].trim());
          });
      });
  }
);

Cypress.Commands.add("open_loan_options", () => {
  cy.get(`.badger-accordion-toggle`).click();
});

// -- This set County field --
Cypress.Commands.add("set_Loan_Term", (state, save) => {
  const STATES = [
    "30 YR Fixed",
    "20 YR Fixed",
    "25 YR Fixed",
    "15 YR Fixed",
    "40 YR Fixed",
    "5/6 ARM",
    "7/6 ARM",
    "10/6 ARM"];
  assert.isString(state);
  expect(state).to.be.oneOf(STATES);
  cy.contains(state)
    .parent()
    .within(() => {
      cy.get('input')
        .check({ force: true });
    });
  if (save) {
    cy.save_Changes();
  }
});
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })


Cypress.Commands.add('deleteUser', (id) => {
  cy.visit(`https://${Cypress.env("url").includes(`/staging`) ? "dev" : "api"}.iqualifi.net/admin/login/`);
  cy.get('#id_username').type(Cypress.env("username"));
  cy.get('#id_password').type(Cypress.env("password"));
  cy.get('.submit-row > input').click();
  cy.visit(`https://${Cypress.env("url").includes(`/staging`) ? "dev" : "api"}.iqualifi.net/admin/users/user/${id}/delete`)
  cy.get('[type="submit"]').click();
});

Cypress.Commands.add('verify_empty table', (id) => {
  let selectedId = !!id ? id : 0;
  cy.get(".empty-table-text").eq(selectedId)
    .should("be.visible")
    .should(
      "contain",
      `Please contact your Lock Desk or Capital Markets Desk for pricing information`
    );
});


Cypress.Commands.add('getAndSavePDF', ({ subject }) => {
  cy.task("gmail:check", {
    options: {
      from: "info@iqualifi.net",
      subject,
      include_attachments: true,
      wait_time_sec: 10, // Poll interval (in seconds).
      max_wait_time_sec: 60, // Maximum poll time (in seconds), after which we'll giveup.
      after: new Date()
    }
  })
    .then(emails => {
      assert.isAtLeast(
        emails.length,
        1,
        "Expected to find at least one email, but none were found!"
      );
      cy.task("save:pdf", emails[0].attachments[0])
    });
});
