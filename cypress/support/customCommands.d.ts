declare namespace Cypress {
    interface Chainable<Subject> {
        login(): Chainable<any>
        logout(): Chainable<any>
        authenticate_email(): Chainable<any>
        submit_Inputs(program: any): Chainable<any>
        send_Emails_E1(): Chainable<any>
        set_Loan_Amount(loan: any): Chainable<any>
        set_LTV(ltv: any): Chainable<any>
        set_gift(bool: any): Chainable<any>
        set_Loan_Term(state: any): Chainable<any>
        set_gift_of_equity(bool: any, gift: any): Chainable<any>
        set_Loan_Purpose(purpose: any): Chainable<any>
        set_first_time_home_buyer(bool: any): Chainable<any>
        set_Occupancy_Type(type: any): Chainable<any>
        set_state_County(county: any, state: any): Chainable<any>
        set_Credit_Score(creditScore: any): Chainable<any>
        set_Reserves(reserves: any): Chainable<any>
        set_Employment(employment: any): Chainable<any>
        set_Document_Type(document: any): Chainable<any>
        set_DTI(dti: any): Chainable<any>
        set_Property_Type(type: any): Chainable<any>
        set_Units(units: any): Chainable<any>
        type_Code(code: any): Chainable<any>
        set_Housing_History(housing: any): Chainable<any>
        set_Ownership(ownership: any): Chainable<any>
        set_Citizenship(citizenship: any): Chainable<any>
        set_Credit_Event(bool: any): Chainable<any>
        set_Escrow_Waiver(bool: any): Chainable<any>
        set_Subordinate_Financing(financing: any, cltv: any, hcltv: any): Chainable<any>
        set_CLTV(cltv: any): Chainable<any>
        set_HCLTV(hcltv: any): Chainable<any>
        get_pricing(email: any, waitForEnd: any): Chainable<any>
        email_Recommended_Program_Summary(email: any): Chainable<any>
        email_Alternative_Program_Summary(email: any): Chainable<any>
        select_Price_By_Table_Value(tableNumber: any, value: any): Chainable<any>
        get_First_Price_By_Table(tableNumber: any): Chainable<any>
        email_Pricing_Summary(): Chainable<any>
        email_Pricing_Scenario_Summary_To_AE(email: any): Chainable<any>
        email_Pricing_Pre_Quad(email: any): Chainable<any>
        email_which_Programs(email: any): Chainable<any>
        set_Pricing_Tear(tear: any): Chainable<any>
        set_Mark_Up(markup: any): Chainable<any>
        invite_Guest(guest: any): Chainable<any>
        verify_Recommended_Program_Name(names: any): Chainable<any>
        verify_Alternative_Program_Name(alternativesList: any): Chainable<any>
        set_Price_Option_Amortizing(opt: any): Chainable<any>
        set_Price_Option_Fees(opt: any): Chainable<any>
        set_Rate_Sheet(opt: any): Chainable<any>
        set_Price_Option_Show_Price_By(price: any): Chainable<any>
        set_Price_Option_Lock_Days(days: any, wait: any): Chainable<any>
        set_Prospect_Type(type: any, wait: any): Chainable<any>
        set_Accounts(type: any, wait: any): Chainable<any>
        increase_LPC_Equal_Value(wait: any): Chainable<any>
        decrease_LPC_Equal_Value(wait: any): Chainable<any>
        enter_Client_Mode(): Chainable<any>
        enter_Employee_Mode(): Chainable<any>
        set_Prepayment_Penalty(ppp: any, save: any): Chainable<any>
        save_Changes(): Chainable<any>
        set_Pricing_Type(type: any, save: any): Chainable<any>
        verify_First_Table_with_All_prices_CUSTOM(programs: any, columnsIndexes: any, HEADERS: any): Chainable<any>
        verify_Second_Table_with_All_prices_CUSTOM(programs: any, columnsIndexes: any, HEADERS2: any): Chainable<any>
        verify_Second_Table_Payments_CUSTOM(columnsIndexes: any, HEADERS: any): Chainable<any>
        open_loan_options(): Chainable<any>
  }
}