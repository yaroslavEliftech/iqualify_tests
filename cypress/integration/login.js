/// <reference types= "Cypress" />
const email = "brokeriqualifi@gmail.com";

describe("Login", () => {
  it("Login with correct email", () => {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(email, false, false);
    cy.contains("h1", "Success!").should("be.visible");
  });

  it("Login with incorrect email", () => {
    const email = "1715sqw2e3d@svvtech.com";

    cy.login();
    cy.submit_Inputs("J10");
    cy.get("#get-pricing-rec").click();
    cy.get("input[type=text][name=email]").type(email);
    cy.get("button[type=submit]").click();
    cy.contains("Well this is awkward...").should("be.visible");
    cy.contains("CONTACT").should("be.visible");
  });

  it("Login with incorrect email and known domain", () => {
    const email = `sqw2e3d${Cypress.env("url").includes(`/staging`) ? "@eliftech.com" : "@pacbell.com"
      }`;

    cy.login();
    cy.submit_Inputs("J10");
    cy.get("#get-pricing-rec").click();
    cy.get("input[type=text][name=email]").type(email);
    cy.get("button[type=submit]").click();
    cy.contains("Let’s create your iQualifi account.").should("be.visible");
  });

  it("Logout", () => {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.contains(`LOG OUT`).click();
    cy.get(".notification")
      .should("be.visible")
      .should(
        `contain`,
        "This action will clear all current selections and return you to the input page, Are you sure?"
      );
    cy.get(`div.no > span`).click();
    cy.contains(`LOG OUT`).click();
    cy.get(".notification").should("be.visible");
    cy.get(`div.yes > span`).click();
    cy.get("#credit-group").should("be.visible");
  });
});
