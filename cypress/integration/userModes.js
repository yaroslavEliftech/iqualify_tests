const HEADERS = [
  " Show: Points  Price ",
  " PROGRAM ",
  " 40 YR Fixed (I/O req) ",
  " Monthly Payment ",
];

const HEADERS_30 = [
  " Show: Points  Price ",
  " PROGRAM ",
  " 30 year Fixed ",
  " Monthly Payment ",
];

describe("Different roles", () => {
  it("Enter SU mode", () => {
    const TYPES = [
      "Broker",
      "Correspondent",
      "Commercial",
      "DTC",
      "Joint Venture",
      "Jvm",
      "Distributed Retail",
      "Distributed Retail Manager"
    ];
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('be.visible'); //Access to client search bar Access to Client Mode
    cy.contains(`Pricing Type`).should('be.visible'); //Rates & Points or LPC
    cy.contains(`Invite Guest`).should('be.visible');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('be.visible');//Access to Change Prospect Type
    //prospect type
    cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
      cy.get(".dropdown-body").click();
    });
    cy.get(".dropdown-menu.visible > ul > li").then((users) => { //Rate Sheet Types Available ALL
      expect(users.length).to.be.equal(8);
      TYPES.forEach((item, index) => {
        cy.wrap(users[index]).should('contain', item);
      })
    })
    cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
      cy.get(".dropdown-body").click();
    });
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
    cy.set_Pricing_Type("LPC", true);
    cy.get(`.user-info-lpc`).should('be.visible'); //Access to change LPC
    cy.get('.incr-btn').should('be.visible');//Access to change LPC
    cy.get('.decrement-btn').should('be.visible');//Access to change LPC
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);

  });

  it("Enter Recruiter mode", () => {
    const TYPES = [
      "Broker",
      "Correspondent",
      "Commercial",
      "DTC",
      "Distributed Retail"
    ];
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`recruiter.iqualifi@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('be.visible'); //Access to client search bar Access to Client Mode
    cy.contains(`Pricing Type`).should('be.visible'); //Rates & Points or LPC
    cy.contains(`Invite Guest`).should('be.visible');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('be.visible');//Access to Change Prospect Type
    //prospect type
    cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
      cy.get(".dropdown-body").click();
    });
    cy.get(".dropdown-menu.visible > ul > li").then((users) => { //Rate Sheet Types Available ALL
      expect(users.length).to.be.equal(5);
      TYPES.forEach((item, index) => {
        cy.wrap(users[index]).should('contain', item);
      })
    })
    cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
      cy.get(".dropdown-body").click();
    });
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
    cy.set_Pricing_Type("LPC", true);
    cy.get(`.user-info-lpc`).should('be.visible'); //Access to change LPC
    cy.get('.incr-btn').should('be.visible');//Access to change LPC
    cy.get('.decrement-btn').should('be.visible');//Access to change LPC
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });

  it("Enter Account Executive (AE) mode", () => {
    const TYPES = [
      "Broker",
      "Correspondent",
      "Commercial",
    ];
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`testaequalifi@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('be.visible'); //Access to client search bar Access to Client Mode
    cy.contains(`Pricing Type`).should('be.visible'); //Rates & Points or LPC
    cy.contains(`Invite Guest`).should('be.visible');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('be.visible');//Access to Change Prospect Type
    //prospect type
    cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
      cy.get(".dropdown-body").click();
    });
    cy.get(".dropdown-menu.visible > ul > li").then((users) => { //Rate Sheet Types Available ALL
      expect(users.length).to.be.equal(3);
      TYPES.forEach((item, index) => {
        cy.wrap(users[index]).should('contain', item);
      })
    })
    cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
      cy.get(".dropdown-body").click();
    });
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
    cy.set_Pricing_Type("LPC", true);
    cy.get(`.user-info-lpc`).should('be.visible'); //Access to change LPC
    cy.get('.incr-btn').should('be.visible');//Access to change LPC
    cy.get('.decrement-btn').should('be.visible');//Access to change LPC
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });

  it("Enter Account Executive Assistant mode", () => {
    const TYPES = [
      "Broker",
      "Correspondent",
      "Commercial",
    ];
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`aeassistantiqulify@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('be.visible'); //Access to client search bar Access to Client Mode
    cy.contains(`Pricing Type`).should('be.visible'); //Rates & Points or LPC
    cy.contains(`Invite Guest`).should('be.visible');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('be.visible');//Access to Change Prospect Type
    //prospect type
    cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
      cy.get(".dropdown-body").click();
    });
    cy.get(".dropdown-menu.visible > ul > li").then((users) => { //Rate Sheet Types Available ALL
      expect(users.length).to.be.equal(3);
      TYPES.forEach((item, index) => {
        cy.wrap(users[index]).should('contain', item);
      })
    })
    cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
      cy.get(".dropdown-body").click();
    });
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
    cy.set_Pricing_Type("LPC", true);
    cy.get(`.user-info-lpc`).should('be.visible'); //Access to change LPC
    cy.get('.incr-btn').should('be.visible');//Access to change LPC
    cy.get('.decrement-btn').should('be.visible');//Access to change LPC
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });

  it("Enter Distributed Retail Manager (DRM) mode", () => {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`dmiqualifi@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('not.be.exist'); //Access to client search bar Access to Client Mode

    cy.contains(`Invite Guest`).should('be.visible');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('not.be.exist');//Access to Change Prospect Type
    cy.get(`.user-info-lpc`).should('be.visible'); //Access to change DR Tier Toggle
    cy.get(`.user-info-data > :nth-child(1) > p`).should('contain', "TIER"); // Access to change DR Tier Toggle
    cy.get('.incr-btn').should('be.visible');// Access to change DR Tier Toggle
    //cy.get('.decrement-btn').should('be.visible');// Access to change DR Tier Toggle
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });

  it("Enter Commercial Broker (COMM) mode", () => {
    cy.login();
    cy.submit_Inputs("I10");
    cy.get_pricing(`commercialBrokeriqulify@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('not.be.exist'); //Access to client search bar Access to Client Mode

    cy.contains(`Invite Guest`).should('not.be.exist');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('not.be.exist');//Access to Change Prospect Type
    cy.get(`.user-info-lpc`).should('be.visible').and("contain", "Tier 1");  //Tier info
    cy.contains(`.incr-btn`).should('not.be.exist');//Access to Change Prospect Type
    cy.verify_First_Table_with_All_prices_CUSTOM(["I10"], [0, 2], HEADERS_30);
  });

  it("Enter Correspondent (CORR) mode", () => {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`correspondentiqulify@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('not.be.exist'); //Access to client search bar Access to Client Mode

    cy.contains(`Invite Guest`).should('not.be.exist');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('not.be.exist');//Access to Change Prospect Type
    cy.get(`.user-info-lpc`).should('be.visible').and("contain", "Tier 4");  //Tier info
    cy.contains(`.incr-btn`).should('not.be.exist');//Access to Change Prospect Type
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });

  it("Enter Wholesale Broker (WH) mode", () => {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`brokeriqualifi@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('not.be.exist'); //Access to client search bar Access to Client Mode
    cy.contains(`Pricing Type`).should('be.visible'); //Rates & Points or LPC

    cy.contains(`Invite Guest`).should('not.be.exist');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('not.be.exist');//Access to Change Prospect Type
    cy.get(`.user-info-lpc`).should('not.be.exist');  //Access to change LPC //TODO ADD verif
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
    cy.set_Pricing_Type("LPC", true);
    cy.get(`.user-info-lpc`).should('be.visible'); //Access to change LPC
    cy.get('.user-info-lpc > .content').should('contain', "2.750 %");
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });

  it("Enter DTC (Agency1) mode", () => {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`dtciqualifi@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('not.be.exist'); //Access to client search bar Access to Client Mode

    cy.contains(`Invite Guest`).should('not.be.exist');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('not.be.exist');//Access to Change Prospect Type
    cy.get(`.user-info-lpc`).should('not.be.exist');  //Access to change LPC //TODO ADD verif
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });

  it("Enter Joint Venture Manager (jvm) mode", () => {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`jvmiqulify@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('not.be.exist'); //Access to client search bar Access to Client Mode

    cy.contains(`Invite Guest`).should('be.visible');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('not.be.exist');//Access to Change Prospect Type
    cy.get(`.user-info-lpc`).should('not.be.exist');  //Access to change LPC //TODO ADD verif
    cy.get('.display-settings-container > .pricing-table-options-toggle > .select-container > .switch-container').should('be.visible'); //Toggle to Jv douglas eliman & jv Retail"
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });

  it("Enter Joint Venture(jv) mode", () => {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`jvretail@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('not.be.exist'); //Access to client search bar Access to Client Mode

    cy.contains(`Invite Guest`).should('not.be.exist');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('not.be.exist');//Access to Change Prospect Type
    cy.get(`.user-info-lpc`).should('not.be.exist');  //Access to change LPC //TODO ADD verif
    cy.get('.single-jv-rate-type > .content').should("contain", "Retail");
    cy.get('.display-settings-container > .pricing-table-options-toggle > .select-container > .switch-container').should('not.be.exist'); //Toggle to Jv douglas eliman & jv Retail"
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });

  it("Enter Distributed Retail (DR) mode", () => {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`distretailiqualifi@gmail.com`);
    cy.get(`[placeholder="Search for a company..."]`).should('not.be.exist'); //Access to client search bar Access to Client Mode

    cy.contains(`Invite Guest`).should('not.be.exist');//Access to Guest Pass
    cy.contains(`PROSPECT TYPE`).should('not.be.exist');//Access to Change Prospect Type
    cy.get(`.user-info-lpc`).should('be.visible'); //Access to change DR Tier Toggle
    cy.get('.user-info-lpc > .content').should('contain', "Tier 3"); // Access to change DR Tier Toggle
    cy.get('.display-settings-container > .pricing-table-options-toggle > .select-container > .switch-container').should('not.be.exist'); //Toggle to Jv douglas eliman & jv Retail"
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });

  describe("Enter Freelancer Account mode", () => {
    const ACCOUNTS = [
      "Eliftech DR (Tier 1)",
      "Eliftech Sprout Correspondent (Tier 5)",
      "Standard Capital Mortgage Corp. WPG2 2.750 (Broker)",
    ];
    const TYPES = [
      "Broker",
      "Correspondent",
      "Commercial",
    ];
    it("Enter Freelancer Account mode on the first account", () => {
      cy.login();
      cy.submit_Inputs("J10");
      cy.get_pricing(`freelanceiqulify@gmail.com`);
      cy.get(`[placeholder="Search for a company..."]`).should('be.visible'); //Access to client search bar Access to Client Mode
      cy.contains(`Invite Guest`).should('be.visible');//Access to Guest Pass
      cy.contains(`ACCOUNTS`).should('be.visible');//Access to Change Prospect Type
      //Accounts
      cy.contains(`ACCOUNTS`).parent().parent().within(() => {
        cy.get(".dropdown-body").click();
      })
      cy.get(".dropdown-menu.visible > ul > li").then((users) => { //Rate Sheet Types Available ALL
        expect(users.length).to.be.equal(3);
        ACCOUNTS.forEach((item, index) => {
          cy.wrap(users[index]).should('contain', item);
        })
      })
      cy.contains(`ACCOUNTS`).parent().parent().within(() => {
        cy.get(".dropdown-body").click();
      });
      cy.get(`.user-info-lpc`).should('be.visible'); //Access to change DR Tier Toggle
      cy.get(`.user-info-data`).should('contain', "TIER"); // Access to change DR Tier Toggle
      cy.get('.incr-btn').should('be.visible');// Access to change DR Tier Toggle
      cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
    });
    it("Enter Correspondent Account", () => {
      cy.set_Accounts(`Sprout Correspondent`, true);
      cy.get(`[placeholder="Search for a company..."]`).should('be.visible'); //Access to client search bar Access to Client Mode
      cy.contains(`Invite Guest`).should('be.visible');//Access to Guest Pass
      cy.contains(`ACCOUNTS`).should('be.visible');//Access to Change Prospect Type
      //Accounts
      cy.contains(`ACCOUNTS`).parent().parent().within(() => {
        cy.get(".dropdown-body").click();
      });
      cy.get(".dropdown-menu.visible > ul > li").then((users) => { //Rate Sheet Types Available ALL
        expect(users.length).to.be.equal(3);
        ACCOUNTS.forEach((item, index) => {
          cy.wrap(users[index]).should('contain', item);
        })
      });
      cy.contains(`ACCOUNTS`).parent().parent().within(() => {
        cy.get(".dropdown-body").click();
      });
      //PROSPECT TYPE
      cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
        cy.get(".dropdown-body").click();
      });
      cy.get(".dropdown-menu.visible > ul > li").then((users) => { //Rate Sheet Types Available ALL
        expect(users.length).to.be.equal(3);
        TYPES.forEach((item, index) => {
          cy.wrap(users[index]).should('contain', item);
        })
      });
      cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
        cy.get(".dropdown-body").click();
      });
      cy.get(`.user-info-lpc`).should('be.visible'); //Access to change Tier Toggle
      cy.get(`.user-info-data`).should('contain', "TIER"); // Access to change Tier Toggle
      cy.get('.incr-btn').should('be.visible');// Access to change Tier Toggle
      cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
    });

    it("Enter Broker Account (Assistant)", () => {
      cy.set_Accounts(`Standard Capital Mortgage Corp`, true);
      cy.get(`[placeholder="Search for a company..."]`).should('be.visible'); //Access to client search bar Access to Client Mode
      cy.contains(`Pricing Type`).should('be.visible'); //Rates & Points or LPC
      cy.contains(`Invite Guest`).should('be.visible');//Access to Guest Pass
      cy.contains(`ACCOUNTS`).should('be.visible');//Access to Change Prospect Type
      //Accounts
      cy.contains(`ACCOUNTS`).parent().parent().within(() => {
        cy.get(".dropdown-body").click();
      });
      cy.get(".dropdown-menu.visible > ul > li").then((users) => { //Rate Sheet Types Available ALL
        expect(users.length).to.be.equal(3);
        ACCOUNTS.forEach((item, index) => {
          cy.wrap(users[index]).should('contain', item);
        })
      });
      cy.contains(`ACCOUNTS`).parent().parent().within(() => {
        cy.get(".dropdown-body").click();
      });
      //PROSPECT TYPE
      cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
        cy.get(".dropdown-body").click();
      });
      cy.get(".dropdown-menu.visible > ul > li").then((users) => { //Rate Sheet Types Available ALL
        expect(users.length).to.be.equal(3);
        TYPES.forEach((item, index) => {
          cy.wrap(users[index]).should('contain', item);
        })
      });
      cy.contains(`PROSPECT TYPE`).parent().parent().within(() => {
        cy.get(".dropdown-body").click();
      });
      cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
      cy.set_Pricing_Type("LPC", true);
      cy.get(`.user-info-lpc`).should('be.visible'); //Access to change LPC
      cy.get('.decrement-btn').should('be.visible');//Access to change LPC
      //cy.get('.incr-btn').should('be.visible');//Access to change LPC
      cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
    });
  });
});