/// <reference types= "Cypress" />
const HEADERS = [
  " Show: Points  Price ",
  " PROGRAM ",
  " 30 year Fixed ",
  " Monthly Payment ",
];

describe.skip("M10 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("M10 pricing", function () {
    cy.login();
    cy.submit_Inputs("M10");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.M10["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["M10"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["M10"], [0, 2], HEADERS);
  });
});

describe.skip("R10 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("R10 pricing", function () {
    cy.login();
    cy.submit_Inputs("R10");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.R10["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["R10"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["R10"], [0, 2], HEADERS);
  });
});

describe("E3 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("E3 pricing", function () {
    cy.login();
    cy.submit_Inputs("E3");
    cy.get(":nth-child(6) > div > .product-result-component-text", {
      timeout: 10000,
    })
      .contains(
        "Please note: The iQualifi program recommendation is based on information entered. During application underwriting, additional income documentation may be required for self-employed individuals. HCLTV will be used if the borrower currently has a home equity line of credit."
      )
      .should("be.visible");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.E3["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["E3"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["E3"], [0, 2], HEADERS);
  });
});

describe("E4 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  let HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 5/6 ARM ",
    " Monthly Payment ",
  ];
  it("E4 pricing", function () {
    cy.login();
    cy.submit_Inputs("E4");
    cy.get(":nth-child(6) > div > .product-result-component-text", {
      timeout: 10000,
    })
      .contains(
        "Please note: The iQualifi program recommendation is based on information entered. During application underwriting, additional income documentation may be required for self-employed individuals. HCLTV will be used if the borrower currently has a home equity line of credit."
      )
      .should("be.visible");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.E4["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["E4"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["E4"], [0, 2], HEADERS);
  });
});

describe("J10 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  let HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 40 YR Fixed (I/O req) ",
    " Monthly Payment ",
  ];
  it("J10 pricing", function () {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.J10["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["J10"], [0, 2], HEADERS);
  });
});

describe("J11 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("J11 pricing", function () {
    cy.login();
    cy.submit_Inputs("J11");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.J11["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["J11"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["J11"], [0, 2], HEADERS);
  });
});

describe("J12 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("J12 pricing", function () {
    cy.login();
    cy.submit_Inputs("J12");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.J12["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["J12"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["J12"], [0, 2], HEADERS);
  });
});

describe("A11 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  let HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 40 YR Fixed (I/O req) ",
    " Monthly Payment ",
  ];
  it("A11 pricing", function () {
    cy.login();
    cy.submit_Inputs("A11");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.A11["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["A11"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["A11"], [0, 2], HEADERS);
  });
});

describe("A14 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  let HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 40 YR Fixed (I/O req) ",
    " Monthly Payment ",
  ];
  it("A14 pricing", function () {
    cy.login();
    cy.submit_Inputs("A14", true);
    cy.get_pricing(`employeeiqualifi@gmail.com`,true);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.A14["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["A14"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["A14"], [0, 2], HEADERS);
  });
});

describe("A15 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  const HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 5/6 ARM ",
    " Monthly Payment ",
  ];
  it("A15 pricing", function () {
    cy.login();
    cy.submit_Inputs("A15");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.A15["Full Name"])
      .should("be.visible");
  });
  // it("Set Both tables", function () {
  //   cy.set_Pricing_Type("BOTH", true);
  //   cy.verify_First_Table_with_All_prices_CUSTOM(["A15"], [0, 2], HEADERS);
  //   cy.verify_Second_Table_with_All_prices_CUSTOM(["A15"], [0, 2], HEADERS);
  // });
});

describe("A16 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("A16 pricing", function () {
    cy.login();
    cy.submit_Inputs("A16", true);
    cy.get_pricing(`employeeiqualifi@gmail.com`, true);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.A16["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    HEADERS[2] = " 30 year Fixed ";
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["A16"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["A16"], [0, 2], HEADERS);
  });
});

describe("I10 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  let HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 30 year Fixed ",
    " Monthly Payment ",
  ];
  it("I10 pricing", function () {
    cy.login();
    cy.submit_Inputs("I10");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.I10["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["I10"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["I10"], [0, 2], HEADERS);
  });
});

describe("I12 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  let HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 40 YR Fixed (I/O req) ",
    " Monthly Payment ",
  ];
  it("I12 pricing", function () {
    cy.login();
    cy.submit_Inputs("I12");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.I12["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["I12"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["I12"], [0, 2], HEADERS);
  });
});

describe("I14 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  let HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 40 YR Fixed (I/O req) ",
    " Monthly Payment ",
  ];
  it("I14 pricing", function () {
    cy.login();
    cy.submit_Inputs("I14");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.I14["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["I14"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["I14"], [0, 2], HEADERS);
  });
});

describe("I15 pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  let HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 40 YR Fixed (I/O req) ",
    " Monthly Payment ",
  ];
  it("I15 pricing", function () {
    cy.login();
    cy.submit_Inputs("I15");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.I15["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["I15"], [0, 2], HEADERS);
    cy.verify_Second_Table_with_All_prices_CUSTOM(["I15"], [0, 2], HEADERS);
  });
});

