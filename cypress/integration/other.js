/// <reference types= "Cypress" />
const email = "brokeriqualifi@gmail.com";
const isRetail = Cypress.env("url").includes(`/staging`) || Cypress.env("url").includes(`/retail`);
const FCollection = isRetail ? "F1" : "";
const VCollection = isRetail ? "V1" : "";
const V2Collection = isRetail ? "V3" : "";
const listOfAllPrograms = ["I10", "I11", "I12", "I14", "I15", "E1", "E3", "E4", "J10", "J11", "J12", "A10", "A11", "A14", "A15", "A16", "C1", FCollection, VCollection, V2Collection];

describe("Buddy Page", () => {

  it("Overlay message verification", () => {
    cy.login();
    cy.submit_Inputs("C_Primary");
    cy.get(".overlay-message").should("be.visible");
  });

  it("Near miss summary tables verification", () => {
    cy.loginRetail();
    cy.submit_Inputs("I12");
    cy.contains("SHOW ME WHICH PROGRAMS").click();
    const LOAN_SECTION = [
      "Loan Amount:",
      "LTV",
      "Veteran",
      "Gift",
      "Gift OF Equity"
    ];
    const PROPERTY_SECTION = [
      "Loan Purpose:",
      "Occupancy Type:",
      "First Time Home Buyer:",
      "Property County & State:",
      "Property Type:",
      "Total Units:"
    ];
    const CREDIT_SECTION = [
      "Credit Score:",
      "Housing History:",
      "Reserves:",
      "Ownership:",
      "Employment:",
      "Credit Event History:",
      "Ratios:",
      "Document Type:",
      "Income Type:",
      "Citizenship:",
    ];
    const TABLE_COLUMNS = [
      "Loan Amount",
      "LTV ",
      "Veteran",
      "Gift",
      "Loan Purpose",
      "Occupancy Type",
      "First Time Home Buyer",
      "Property County & State",
      "Property Type",
      "Credit Score",
      "Housing History",
      "Reserves",
      "Ownership",
      "Employment",
      "Credit Event History",
      "Ratios",
      "Document Type",
    ]
    cy.get(".loan-and-property > :nth-child(1) > h1.form-title").should("contain", "Loan"); //header
    LOAN_SECTION.forEach((val, index) => {
      cy.get(`.loan-and-property > :nth-child(1) > :nth-child(${index + 2}) > .form-title`).should("contain", val);
    });

    cy.get(".loan-and-property > :nth-child(2) > h1.form-title").should("contain", "Property"); //header
    PROPERTY_SECTION.forEach((val, index) => {
      cy.get(`.loan-and-property > :nth-child(2) > :nth-child(${index + 2}) > .form-title`).should("contain", val);
    });

    cy.get(".credit > :nth-child(1) > h1.form-title").should("contain", "Credit"); //header
    CREDIT_SECTION.forEach((val, index) => {
      cy.get(`.credit > article > :nth-child(${index + 2}) > .form-title`).should("contain", val);
    });

    //table

    cy.get("thead").within(() => {
      TABLE_COLUMNS.forEach((val, index) => {
        cy.get(`:nth-child(2) > :nth-child(${index + 2})`).should("contain", val);
      });
    });

    cy.get(".pricing-table-near-miss").should(($p) => {
      listOfAllPrograms.forEach((program) => {
        expect($p).to.contain.text(program);
      })
    });
  });

  it("What is a pricing collection? link", () => {
    cy.login();
    cy.submit_Inputs("E1");
    cy.get("small.priceCollectionBtn").should("be.visible");
    cy.get("small.priceCollectionBtn").click();
    cy.get(".pricing-collection-info-modal-content").should("be.visible");
    cy.get(".pricing-collection-info-title").should(
      "contain",
      " What is a pricing collection? "
    );
    cy.get(".pricing-collection-info-title").should(
      "contain",
      "A pricing collection is a grouping of 2 or more loan programs with similar qualifications where iQualifi automatically displays the best prices available –so you don’t have to run your scenario multiple times on several different programs to find the best price. iQualifi checks every rate / price combination that fits your scenario across multiple loan programs and identifies the program with the best price for each rate.  You see one pricing table on your screen, but it will be made up of several different loan programs. Simply select a rate / price combination and you’ll know which loan program is best for your scenario."
    );
  });
});

describe("Inputs page", () => {
  it("Required fields validation", () => {
    cy.login();
    cy.get("div.submit-form-button > button:nth-child(1)").click();
    //LOAN
    cy.get(".group-heading-error")
      .contains("LOAN AMOUNT")
      .parent()
      .within(() => {
        cy.contains("Please enter a value").should("be.visible");
      });

    //PROPERTY
    cy.get(".group-heading-error")
      .contains("LOAN PURPOSE")
      .parent()
      .within(() => {
        cy.contains("Please Choose an Option").should("be.visible");
      });

    cy.get(".group-heading-error")
      .contains("OCCUPANCY TYPE")
      .parent()
      .within(() => {
        cy.contains("Please Choose an Option").should("be.visible");
      });

    cy.get(".group-heading-error")
      .contains("Property County & State")
      .parent()
      .within(() => {
        cy.get("span").not(`[style="display: none;"]`).last().should("have.text", "Type a valid County or State").and("be.visible");
      });

    //CREDIT INFORMATION
    cy.get(".group-heading-error")
      .contains("CREDIT SCORE")
      .parent()
      .within(() => {
        cy.contains("Please select an option").should("be.visible");
      });
  });

  it("Edit scenario", () => {
    cy.login();
    cy.submit_Inputs("J10");

    cy.get(".go-back", {
      timeout: 10000,
    }).click();

    cy.get("#displayedText").clear();
    cy.submit_Inputs("A10");

    cy.get_pricing(email);
    cy.get(".go-back", {
      timeout: 10000,
    }).click();

    cy.get("#displayedText").clear();
    cy.submit_Inputs("J10");
  });
});

describe("Additional tiles feature", () => {
  it("4 tiles verification", () => {
    cy.login();
    cy.submit_Inputs("4_Tiles");
    if (isRetail) {
      //first tile 
      cy.get('#results-additional-1st').should('exist').and("contain", `E5 Best Price Collection`);
      cy.get('#get-pricing-add1').should('exist');
      cy.get('#results-additional-1st > .container > .email-me-pdf-summary > .recommended-inner-button').should('exist');
      //second tile
      cy.get('#results-additional-2nd').should('exist').and("contain", `SP J10 – Select Jumbo Full Doc`);
      cy.get('#get-pricing-add2').should('exist');
      cy.get('#results-additional-2nd > .container > .email-me-pdf-summary > .recommended-inner-button').should('exist');
    } else {
      //first tile 
      cy.get('#results-additional-1st').should('exist').and("contain", `SP J10 – Select Jumbo Full Doc`);
      cy.get('#get-pricing-add1').should('exist');
      cy.get('#results-additional-1st > .container > .email-me-pdf-summary > .recommended-inner-button').should('exist');
    }
  });

  it("Navigate to Near Miss and verify Get pricing", () => {
    cy.get('.base > .recommended-inner-button').click();
    cy.get('.recommended > span').should('exist');
    cy.get('.alternative').should('exist');
    cy.get('.program-name-container > .additional').should('have.length', isRetail ? 2 : 1);
  });
});

describe("Change roles with the SU account", () => {
  it("Enter Client mode", () => {
    cy.login();
    cy.submit_Inputs("I10");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.enter_Client_Mode();
    cy.enter_Employee_Mode();
  });

  it("Enter Correspondent mode", () => {
    cy.set_Prospect_Type(`Correspondent`, false);
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({
          user_prospect_type: "correspondent",
        });
      });
    cy.get(`.mode-text`).should("contain", `Employee mode`);
    cy.get(`.lpc-content`).should("contain", `Tier 4`);
  });

  it("Enter Commercial mode", () => {
    cy.set_Prospect_Type(`Commercial`, false);
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({
          user_prospect_type: "commercial",
        });
      });
    cy.get(`.mode-text`).should("contain", `Employee mode`);
  });

  it("Enter Distributed retail mode", () => {
    cy.set_Prospect_Type(`Distributed Retail`, false);
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({
          user_prospect_type: "distributed retail"
        });
      });
    cy.get(`.mode-text`).should("contain", `Employee mode`);
  });

  it("Enter Distributed retail Manager mode", () => {
    cy.set_Prospect_Type(`Distributed Retail Manager`, false);
    cy.wait("@getPrice").then((interception) => {
      expect(interception.request.body).to.nested.include({
        user_prospect_type: "distributed retail manager",
      });
      expect(interception.response.body.user_data).to.nested.include({
        is_distributed_retail_manager: true,
      });
    });

    cy.get(`.mode-text`).should("contain", `Employee mode`);
  });

  it("Enter Joint Venture mode", () => {
    cy.set_Prospect_Type(`Joint Venture`, false);
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({
          user_prospect_type: "jv"
        });
      });
    cy.get(`.mode-text`).should("contain", `Employee mode`);
  });

  it("Enter Agency mode", () => {
    // cy.login();
    // cy.submit_Inputs("I10");
    // cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.set_Prospect_Type(`DTC`, false);
    cy.wait("@getPrice").then((interception) => {
      expect(interception.request.body).to.nested.include({
        user_prospect_type: "DTC",
      });
    });
    cy.get(`.mode-text`).should("contain", `Employee mode`);
  });
});

describe("PPP feature", () => {
  it("3 Year verification", () => {
    cy.login();
    cy.submit_Inputs("I10");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    });
    cy.set_Pricing_Type("BOTH");
    cy.set_Prepayment_Penalty("3", true);
    cy.get_First_Price_By_Table(0);
    cy.email_Pricing_Summary();
  });

  it("2 Year verification", () => {
    cy.get('.prequal-pdf-modal-container').should('not.exist');
    cy.set_Prepayment_Penalty("2", true);
    cy.get_First_Price_By_Table(0);
    cy.email_Pricing_Summary();
  });
});

describe("Bringing Back CORR Tiers", () => {
  const HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 40 YR Fixed (I/O req) ",
    " Monthly Payment ",
  ];
  it("Default tier", () => {
    cy.login();
    cy.submit_Inputs("A11");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.set_Prospect_Type(`Correspondent`, true);
    cy.get(`.mode-text`).should("contain", `Employee mode`);
    cy.get(`.lpc-content`).should("contain", `Tier 4`);
    const HEADERS = [
      " Show: Points  Price ",
      " PROGRAM ",
      " 40 YR Fixed (I/O req) ",
      " Monthly Payment ",
    ];
    cy.verify_First_Table_with_All_prices_CUSTOM(["A11"], [0, 2], HEADERS);
  });
  it("Send email", () => {
    cy.get_First_Price_By_Table(0);
    cy.email_Pricing_Summary();
    cy.get('.prequal-pdf-modal-container').should('not.exist')
  });

  it("Change tier to TIER 5", () => {
    cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
    cy.get(`.incr-btn`).click({
      force: true,
    });
    cy.wait("@getPrice");
    cy.get(`.lpc-content`).should("contain", `Tier 5`);

    cy.verify_First_Table_with_All_prices_CUSTOM(["A11"], [0, 2], HEADERS);
  });

  it("Send email", () => {
    cy.get_First_Price_By_Table(0);
    cy.email_Pricing_Summary();
    cy.get('.prequal-pdf-modal-container').should('not.exist')
  });
});

describe("Both project ", () => {
  it("Commercial prices", () => {
    const HEADERS = [
      " Show: Points  Price ",
      " PROGRAM ",
      " 30 year Fixed ",
      " Monthly Payment ",
    ];
    cy.login();
    cy.submit_Inputs("I10");
    cy.get_pricing(`both1iqualifi@gmail.com`);
    cy.get(`.lpc-content`).should("contain", `Tier 1`);

    cy.verify_First_Table_with_All_prices_CUSTOM(["I10"], [0, 2], HEADERS);
  });
  it("Send email", () => {
    cy.get_First_Price_By_Table(0);
    cy.email_Pricing_Summary();
    cy.get('.prequal-pdf-modal-container').should('not.exist')
  });

  it("Change RATE SHEET to Correspondent prices", () => {
    const HEADERS = [
      " Show: Points  Price ",
      " PROGRAM ",
      " 30 year Fixed ",
      " Monthly Payment ",
    ];
    cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
    cy.set_Rate_Sheet(`Correspondent`);
    cy.contains(`UPDATE PRICING TABLE`).click();
    cy.wait("@getPrice");
    cy.get(`.lpc-content`).should("contain", `Tier 4`);

    cy.verify_First_Table_with_All_prices_CUSTOM(["I10"], [0, 2], HEADERS);
  });

  it("Send email", () => {
    cy.get_First_Price_By_Table(0);
    cy.email_Pricing_Summary();
    cy.get('.prequal-pdf-modal-container').should('not.exist')
  });
});

describe("Remove text from a decline reason feature", () => {
  it("Get program with the message", () => {
    cy.login();
    cy.fixture("programs.json").then((programs) => {
      const currentProgram = programs.J10;
      cy.set_Loan_Amount(4000000);
      cy.set_LTV(currentProgram["LTV"]);
      cy.set_Loan_Purpose(currentProgram["Loan Purpose"]);
      cy.set_Occupancy_Type(currentProgram["Occupancy Type"]);
      cy.set_state_County(`Bristol County`, `MA`);
      cy.set_Property_Type(currentProgram["Property Type"]);
      cy.set_Units(currentProgram["Units"]);
      cy.set_Credit_Score(currentProgram["Credit Score"]);
      cy.set_Housing_History(programs.E1["Housing History"]);
      cy.set_Reserves(currentProgram["Reserves"]);
      cy.set_Employment(currentProgram["Employment"]);
      cy.set_Document_Type(currentProgram["Document Type"]);
      cy.set_DTI(currentProgram["DTI"]);
      cy.get("button").contains("SUBMIT").click();
      cy.wait("@Options");
      cy.verify_Recommended_Program_Name(currentProgram["Full Name"]);
      cy.verify_Alternative_Program_Name(currentProgram["Alternative Program"]);
    });
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.set_Pricing_Type("BOTH", true);
    cy.get(".pricing-table-container > .no-pricing-container")
      .eq(0)
      .should("be.visible")
      .should(`contain`, "Oops...")
      .should(
        `contain`,
        "Looks like your state selection doesn’t meet the guidelines. You can always edit your scenario or contact your Account Executive for alternative options."
      );
    cy.get(".pricing-table-container > .no-pricing-container")
      .eq(1)
      .should("be.visible")
      .should(`contain`, "Oops...")
      .should(
        `contain`,
        "Looks like your state selection doesn’t meet the guidelines. You can always edit your scenario or contact your Account Executive for alternative options."
      );
    cy.get(".pricing-table-container .send-mail-no-pricing")
      .eq(0)
      .should("be.visible")
      .should(`contain`, "SEND EMAIL");
    cy.get(".pricing-table-container .send-mail-no-pricing")
      .eq(1)
      .should("be.visible")
      .should(`contain`, "SEND EMAIL");
  });
});

describe("Sorting for C & E programs", () => {
  it("Sorting for E1", () => {
    let previousValue = 0;
    cy.login();
    cy.submit_Inputs("E1");
    cy.get_pricing(`employeeiqualifi@gmail.com`);

    //Verify first table
    cy.get(`:nth-child(1) > .pricing-table tr`)
      .not(`[style="display: none;"]`)
      .each(($row) => {
        cy.wrap($row).within(() => {
          cy.get("td").each(($cell, index) => {
            if (index === 4) {
              cy.wrap($cell)
                .invoke("text")
                .then(parseFloat)
                .should((value) => {
                  assert.isAtMost(previousValue, value);
                  assert.isNumber(value);
                  assert.isNotNaN(value);
                  previousValue = value;
                });
            }
          });
        });
      });
  });
  it("Change sorting to opposite", () => {
    let previousValue = 0;
    cy.get(".sort-icon").click();
    //Verify first table
    cy.get(`:nth-child(1) > .pricing-table tr`)
      .not(`[style="display: none;"]`)
      .each(($row) => {
        cy.wrap($row).within(() => {
          cy.get("td").each(($cell, index) => {
            if (index === 4) {
              cy.wrap($cell)
                .invoke("text")
                .then(parseFloat)
                .should((value) => {
                  if (previousValue === 0) {
                    previousValue = value;
                  }
                  assert.isAtMost(value, previousValue);
                  assert.isNumber(value);
                  assert.isNotNaN(value);
                  previousValue = value;
                });
            }
          });
        });
      });
  });
});

describe("Prices and Points", () => {
  const points = [];
  it("Points for E1", () => {
    cy.login();
    cy.submit_Inputs("E1");
    cy.get_pricing(`employeeiqualifi@gmail.com`);

    //Verify first table
    cy.get(`:nth-child(1) > .pricing-table tr`)
      .not(`[style="display: none;"]`)
      .each(($row) => {
        cy.wrap($row).within(() => {
          cy.get("td").each(($cell, index) => {
            if (index === 0) {
              cy.wrap($cell)
                .invoke("text")
                .then(parseFloat)
                .should((value) => {
                  assert.isNumber(value);
                  assert.isNotNaN(value);
                  points.push(value);
                });
            }
          });
        });
      });
  });
  it("Change to Prices", () => {
    cy.get(".points-price-toggle > :nth-child(2)").click();
    //Verify first table
    cy.get(`:nth-child(1) > .pricing-table tr`)
      .not(`[style="display: none;"]`)
      .each(($row, rowIndex) => {
        cy.wrap($row).within(() => {
          cy.get("td").each(($cell, index) => {
            if (index === 0) {
              cy.wrap($cell)
                .invoke("text")
                .then(parseFloat)
                .should((value) => {
                  assert.isNumber(value);
                  assert.isNotNaN(value);
                  assert.equal(value, ((points[rowIndex] - 100) * -1).toFixed(3));
                });
            }
          });
        });
      });
  });
});

describe("40y/10y fixed column for Interest only ", () => {
  const HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 30 year Fixed ",
    " Monthly Payment ",
  ];
  it("Default Table for A11", () => {
    cy.login();
    cy.submit_Inputs("A11");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.set_Loan_Term("30 YR Fixed", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["A11"], [0, 2], HEADERS);
  });
  it("Change to 40 Yr/Fixed", () => {
    const HEADERS = [
      " Show: Points  Price ",
      " PROGRAM ",
      " 40 YR Fixed (I/O req) ",
      " Monthly Payment ",
    ];
    cy.set_Loan_Term("40 YR Fixed", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(["A11"], [0, 2], HEADERS);
  });
});

describe("Change controls: Amortizing, Fees, Show Price By, Lock Days, LPC, Prospect Type", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("Get J10 Program", function () {
    cy.login();
    cy.submit_Inputs("J10");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.J10["Full Name"])
      .should("be.visible");
  });

  it("Change controls: Both", function () {
    cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
    cy.set_Price_Option_Lock_Days("30");
    cy.set_Pricing_Type("BOTH");
    cy.contains(`UPDATE PRICING TABLE`).click();
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({ selected_pricing_type: "BOTH" });
      });
  });
  it("Change controls: Set Price options", function () {
    cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
    cy.set_Loan_Term("30 YR Fixed")
    cy.set_Price_Option_Amortizing("Amortizing");
    cy.contains(`UPDATE PRICING TABLE`).click();
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({ show_pricing_options: "amortized" });
      });
  });

  it("Change controls: Set Price options Fees", function () {
    cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
    cy.set_Price_Option_Fees("Fees in");
    cy.contains(`UPDATE PRICING TABLE`).click();
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({ show_fees_type_value: "fees in" });
      });
  });

  it("Change controls: Escrow Waiver", function () {
    cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
    cy.set_Escrow_Waiver("Yes");
    cy.contains(`UPDATE PRICING TABLE`).click();
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({ escrow_waiver: "Yes" });
      });
  });

  it("Change controls: Lock Days", function () {
    cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
    cy.set_Price_Option_Show_Price_By("0.125");
    cy.set_Price_Option_Lock_Days("45");
    cy.contains(`UPDATE PRICING TABLE`).click();
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({ lock_period_days: "45-day lock" });
      });
  });

  it("Change controls: Increase LPC", function () {
    cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
    cy.set_Pricing_Type("LPC");
    cy.get(".user-info-lpc > .content")
      .invoke("text")
      .then(parseFloat)
      .then((value) => {
        cy.increase_LPC_Equal_Value();
        cy.wait("@getPrice")
          .its("request.body")
          .should(($body) => {
            expect($body).to.nested.include({
              employee_mode_lpc: value + 0.125,
            });
          });
      });
  });

  it("Change controls: Change Prospect type", function () {
    cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
    cy.set_Prospect_Type(`Correspondent`);
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({
          user_prospect_type: "correspondent",
        });
      });
  });
});


describe("E collection MI controls ", () => {
  const HEADERS = [
    " Show: Points  Price ",
    " PROGRAM ",
    " 30 year Fixed ",
    " Monthly Payment ",
  ];
  const programs = ["E1", "E2", "E5", "E6", "E7", "E8", "E9", "E11"]
  const columnsIndexes = [0, 2]
  it("Default Table for E collection wit MI", () => {
    cy.login();
    cy.submit_Inputs("E+MI");
    cy.get_pricing(`employeeiqualifi@gmail.com`);

    //Verify first table
    cy.get(`:nth-child(1) > .pricing-table tr`)
      .not(`[style="display: none;"]`)
      .each(($row) => {
        cy.wrap($row).within(() => {
          cy.get("td").each(($cell, index) => {
            if (columnsIndexes.includes(index)) {
              cy.wrap($cell)
                .invoke("text")
                .then((text) => {
                  if (text.includes('MI')) {
                    cy.wrap(text.replaceAll(/\+ MI/g, ''))
                  } else {
                    cy.wrap(text)
                  }
                })
                .then(parseFloat)
                .should((value) => {
                  assert.isNumber(value);
                  assert.isNotNaN(value);
                });
            }
            if (index === 1) {
              cy.wrap($cell)
                .invoke("text")
                .should((value) => {
                  assert.isString(value);
                  assert.oneOf(value.trim(), programs);
                });
            }
          });
        });
      });
    //Verify first table headers
    cy.get(`:nth-child(1) > .pricing-table thead td`)
      .each(($row, index) => {
        cy.wrap($row)
          .invoke("text")
          .should((value) => {
            assert.isString(value);
            assert.equal(value.trim(), HEADERS[index].trim());
          });
      });
  });
  it("Change to Interest Only", () => {
    cy.intercept("/iqualifi-api/api/v1/get-pricing/").as("getPrice");
    const HEADERS = [
      " Show: Points  Price ",
      " PROGRAM ",
      " 30 year Fixed ",
      " Monthly Payment ",
    ];
    cy.set_Mortgage_Insurance("Yes");
    cy.contains(`UPDATE PRICING TABLE`).click();
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({ mortgage_insurance: "Yes" });
      });
    cy.verify_First_Table_with_All_prices_CUSTOM(programs, columnsIndexes, HEADERS);
  });
});


describe("Change controls: SUBORDINATE FINANCING HELOC, HELOAN", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("Change controls: SUBORDINATE FINANCING HELOC, HELOAN", function () {
    cy.login();
    cy.submit_Inputs("C_Primary");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.C_Primary["Full Name"])
      .should("be.visible");
    cy.set_Subordinate_Financing("HELOAN", 55);
    cy.contains(`UPDATE PRICING TABLE`).click();
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({ subordinate_financing: "HELOAN" });
        expect($body).to.nested.include({ cltv: 55 });
      });
    cy.set_Subordinate_Financing("HELOC", 55, 86);
    cy.contains(`UPDATE PRICING TABLE`).click();
    cy.wait("@getPrice")
      .its("request.body")
      .should(($body) => {
        expect($body).to.nested.include({ subordinate_financing: "HELOC" });
        expect($body).to.nested.include({ cltv: 55 });
        expect($body).to.nested.include({ hcltv: 86 });
      });
  });
});