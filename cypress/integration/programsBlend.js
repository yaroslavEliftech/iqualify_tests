/// <reference types= "Cypress" />
const email = "brokeriqualifi@gmail.com";
const eCollection = ["E1", "E2", "E5", "E6", "E7", "E8", "E9", "E11"];
const cCollection = ["C1", "C2", "C8", "C9"];
const cCollection_Investment = ["C1", "C2", "C3", "C4"];
const cCollection_Second = ["C1", "C2", "C4", "C5"];
const fCollection = ["F1", "F2"];
const vCollection = ["V1", "V2"];
const v2Collection = ["V3", "V4"];
const aCollection = ["A10", "A25"];
const HEADERS_PAYMENT = [
  " Show: Points  Price ",
  " PROGRAM ",
  " 30 year Fixed ",
  " Monthly Payment ",
];

describe("A pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("A pricing", function () {
    cy.login();
    cy.submit_Inputs("A10");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.A10["Full Name"])
      .should("be.visible");
  });

  it("Verify Payment Tables", function () {
    cy.verify_First_Table_with_All_prices_CUSTOM(aCollection, [0, 2], HEADERS_PAYMENT);
  });
});

describe("F Collection pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });

  it("F Collection pricing", function () {
    cy.loginRetail();
    cy.submit_Inputs("F_Collection");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.F_Collection["Full Name"])
      .should("be.visible");
  });

  it("Verify tables", function () {
    cy.set_Prospect_Type("DTC", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(fCollection, [0, 2], HEADERS_PAYMENT);
  });
});

describe("V Collection pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });

  it("V Collection pricing", function () {
    cy.loginRetail();
    cy.submit_Inputs("V_Collection");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.V_Collection["Full Name"])
      .should("be.visible");
  });

  it("Verify tables", function () {
    cy.set_Prospect_Type("DTC", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(vCollection, [0, 2], HEADERS_PAYMENT);
  });
});

describe("V2 Collection pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });

  it("V2 Collection pricing", function () {
    cy.loginRetail();
    cy.submit_Inputs("V2_Collection");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.V2_Collection["Full Name"])
      .should("be.visible");
  });

  it("Verify tables", function () {
    cy.set_Prospect_Type("DTC", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(v2Collection, [0, 2], HEADERS_PAYMENT);
  });
});


describe("E pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("E pricing", function () {
    cy.login();
    cy.submit_Inputs("E1");
    cy.get(":nth-child(6) > div > .product-result-component-text", {
      timeout: 10000,
    })
      .contains(
        "Please note: The iQualifi program recommendation is based on information entered."
      )
      .should("be.visible");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.E1["Full Name"])
      .should("be.visible");
  });

  it("Verify Payment Tables", function () {
    const HEADERS_PAYMENT_2 = [
      " PTS to SPROUT ",
      " 40 year Fixed ",
      " Monthly Payment ",
    ]
    cy.verify_First_Table_with_All_prices_CUSTOM(eCollection, [0, 2, 3], HEADERS_PAYMENT);
    cy.verify_Second_Table_Payments_CUSTOM([0, 1, 2], HEADERS_PAYMENT_2);
  });

  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(eCollection, [0, 2, 3], HEADERS_PAYMENT);
    cy.verify_Second_Table_with_All_prices_CUSTOM(eCollection, [0, 2, 3], HEADERS_PAYMENT);
  });
});

describe.only("C Collection Primary pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("C Collection Primary pricing", function () {
    cy.login();
    cy.submit_Inputs("C_Primary");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.C_Primary["Full Name"])
      .should("be.visible");
  });

  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(cCollection, [0, 2, 3], HEADERS_PAYMENT);
    cy.verify_Second_Table_with_All_prices_CUSTOM(cCollection, [0, 2, 3], HEADERS_PAYMENT);
  });
});

describe("C Collection Second Home pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("C Collection Second Home pricing", function () {
    cy.login();
    cy.submit_Inputs("C_Second_Home");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.C_Second_Home["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(cCollection_Second, [0, 2, 3], HEADERS_PAYMENT);
    cy.verify_Second_Table_with_All_prices_CUSTOM(cCollection_Second, [0, 2, 3], HEADERS_PAYMENT);
  });
});

describe("C Collection Investment pricing", () => {
  before(() => {
    cy.fixture("programs.json").as("programsJSON");
  });
  after(() => {
    cy.logout();
  });
  it("C Collection Investment pricing", function () {
    cy.login();
    cy.submit_Inputs("C_Investment");
    cy.get_pricing(`employeeiqualifi@gmail.com`);
    cy.get(".strategy-name-container > span", {
      timeout: 10000,
    })
      .contains(this.programsJSON.C_Investment["Full Name"])
      .should("be.visible");
  });
  it("Set Both tables", function () {
    cy.set_Pricing_Type("BOTH", true);
    cy.verify_First_Table_with_All_prices_CUSTOM(cCollection_Investment, [0, 2, 3], HEADERS_PAYMENT);
    cy.verify_Second_Table_with_All_prices_CUSTOM(cCollection_Investment, [0, 2, 3], HEADERS_PAYMENT);
  });
});