const email = "employeeiqualifi@gmail.com";


describe("Emails", () => {
  it("Email me a program summary", () => {
    cy.login();
    cy.submit_Inputs("C_Primary");
    cy.email_Recommended_Program_Summary(email);
    cy.get('.prequal-pdf-modal-container', { timeout: 10000 }).should('not.exist');
    //cy.getAndSavePDF({ subject: "Program Summary is here" });
    cy.email_Alternative_Program_Summary(email);
    cy.get('.prequal-pdf-modal-container', { timeout: 10000 }).should('not.exist')

  });

  it("Email me a pricing, scenario, pre-quad emails", () => {
    cy.fixture("programs.json").then((programs) => {
      cy.login();
      cy.submit_Inputs("A11");
      cy.get_pricing(`employeeiqualifi@gmail.com`);
      cy.get(".strategy-name-container > span", {
        timeout: 10000,
      })
        .contains(programs.A11["Full Name"])
        .should("be.visible");
      cy.get_First_Price_By_Table(0);
      cy.email_Pricing_Summary();
      cy.get('div[name="slide-fade"] > .submit-pdf-button', {
        timeout: 10000,
      }).should("not.exist");
      cy.email_Pricing_Scenario_Summary_To_AE(email);
      cy.get('div[name="slide-fade"] > .submit-pdf-button', {
        timeout: 10000,
      }).should("not.exist");
      cy.email_Pricing_Pre_Quad();
      cy.get('div[name="slide-fade"] > .submit-pdf-button', {
        timeout: 10000,
      }).should("not.exist");
    });
  });

  it("Email me a near miss summary", () => {
    cy.login();
    cy.submit_Inputs("A11");
    cy.email_which_Programs(email);
    cy.get('.prequal-pdf-modal-container', { timeout: 10000 }).should('not.exist')
  });

  it("Email me a DRM pre-quad emails", () => {
    cy.fixture("programs.json").then((programs) => {
      cy.login();
      cy.submit_Inputs("A11");
      cy.get_pricing(`dmiqualifi@gmail.com`);
      cy.get(".strategy-name-container > span", {
        timeout: 10000,
      })
        .contains(programs.A11["Full Name"])
        .should("be.visible");
      cy.get_First_Price_By_Table(0);
      cy.email_Pricing_Pre_Quad_DR({
        firstName: "John",
        lastName: "Smith",
        address: "Poltava",
        city: "Kyiv",
        state: "California",
        zipCode: "79000",
      });
      cy.get('.prequal-pdf-modal-container', { timeout: 10000 }).should('not.exist')
    });
  });
  describe("Invite Guest and remove", function () {
    it("Invite Guest", function () {
      cy.intercept("/iqualifi-api/api/v1/guest-user/").as("GuestRequest");
      const guest = {
        email: "seher@mailinator.com",
        firstName: "Tanisha",
        lastName: "Knox",
        companyName: "Jensen Curtis Plc",
        phone: "1915624312",
        city: "Lviv",
        state: "Alaska",
        price: {
          A_COLLECTION: "0.1",
          A11: "0.2",
          A14: "0.3",
          A16: "0.7",
          C_COLLECTION: "0.8",
          E_COLLECTION: "0.9",
          E3: "0.11",
          E4: "0.12",
          I10: "0.13",
          I11: "0.23",
          I12: "0.14",
          I14: "0.15",
          I15: "0.16",
          J10: "0.17",
          J11: "0.18",
          J12: "0.19",
          M10: "0.21",
          R10: "0.22",
          F_COLLECTION: "0.23",
        },
      };
      const guestRequestExpectation = {
        A11: 0.01,
        A11: 0.02,
        A14: 0.03,
        A16: 0.07,
        C1: 0.08,
        E1: 0.09,
        E3: 0.011,
        E4: 0.012,
        I10: 0.013,
        I11: 0.023,
        I12: 0.014,
        I14: 0.015,
        I15: 0.016,
        J10: 0.017,
        J11: 0.018,
        J12: 0.019,
        M10: 0.021,
        R10: 0.022,
        F1: 0.023,
        accountExecutiveEmail: "employeeiqualifi@gmail.com",
        acct_curr_comp_per: 0,
        city: "Lviv",
        companyName: "Jensen Curtis Plc",
        confirmEmail: "seher@mailinator.com",
        correspondentTier: "Tier 4",
        email: "seher@mailinator.com",
        firstName: "Tanisha",
        lastName: "Knox",
        phone: "(191) 562-4312",
        singleMarkup: 0,
        userProspectType: "correspondent",
      };
      const guestResponseExpectation = {
        "company_or_account": "Jensen Curtis Plc",
        "comp_plan_percent": 0,
        "comments": "A correspondent account",
        "acct_curr_comp_type": "Correspondent",
        "acct_curr_comp_per": 0,
        "account_executive": "employeeiqualifi@gmail.com",
        "acct_price_group": "Sprout Correspondent (Tier 4)",
        "workphone": "(191) 562-4312",
        "a_collection": 0.01,
        "sp_a11": 0.02,
        "sp_a14": 0.03,
        "sp_a16": 0.07,
        "e_collection": 0.09,
        "sp_e3": 0.011,
        "sp_e4": 0.012,
        "f_collection": 0.023,
        "c_collection": 0.08,
        "sp_i10": 0.013,
        "sp_i11": 0.023,
        "sp_i12": 0.014,
        "sp_i14": 0.015,
        "sp_i15": 0.016,
        "sp_r10": 0.022,
        "sp_m10": 0.021,
        "sp_j10": 0.017,
        "sp_j11": 0.018,
        "sp_j12": 0.019,
        "assisted_ae_email": null
      }
      if (cy.isRetail()) {
        guest.price.V_COLLECTION = "0.24";
        guest.price.V2_COLLECTION = "0.25";
        guestRequestExpectation.V1 = 0.024;
        guestRequestExpectation.V3 = 0.025;
        guestResponseExpectation.v_collection = 0.024;
        guestResponseExpectation.v3v4_collection = 0.025;
      }
      cy.login();
      cy.submit_Inputs("A11");
      cy.get_pricing("employeeiqualifi@gmail.com");
      cy.invite_Guest(guest);
      cy.wait("@GuestRequest")
        .then((guestRequest) => {
          if (guestRequest.response.statusCode === 200) {
            cy.task('setUserId', guestRequest.response.body.pricingInfo.user_id);
            expect(guestRequest.response.body.pricingInfo).to.nested.include(guestResponseExpectation);
          }
          return cy.wrap(guestRequest);
        })
        .should(($guestRequest) => {
          expect($guestRequest.request.body).to.nested.include(guestRequestExpectation);
          expect($guestRequest.response.statusCode).to.eql(200);
        });
      cy.get(".regular-text").should(
        "to.contain",
        "Your guest pass request is complete. An e-mail has been sent to you and your guest with the next steps."
      );
      cy.get(".notification-window-container").as("notificationForm");
      cy.get("@notificationForm").within(() => {
        cy.get('div[name="slide-fade"] > .submit-pdf-button', {
          timeout: 10000,
        }).click();
      });

      cy.get("@notificationForm").within(() => {
        cy.get('.guest-modal-container', { timeout: 10000 }).should('not.exist')
      });
    });
    it("Delete user", function () {
      cy.task('getUserId').then((userId) => {
        if (userId !== undefined) {
          cy.deleteUser(userId);
        }
      });
    });
  });
});
